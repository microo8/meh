# meh
not interesting projects/tests/examples that don't deserve to have own repository. licence: [wtfpl](http://www.wtfpl.net/)

##cztorrent
tried to make an web crawler for [cztorrent.net](https://tracker.cztorrent.net),
it gets the chromium cookies for that site and
then downloads movie torrents that have greather than 7.5 on imdb
it doesn't run very well :/

##dml
an examle "language" that compiles to html inspired by qml. implements an compiler from dml to html.
the properties of objects are declaratively set using js (when an property is changed, all the properties that depend on it change too).
can define classes and child nodes, whitch are also classes, or regular html elements.

##godhmm
an discrete hidden markov model experiment

##pgcsv
rewrite of my old script in python to golang that imports an csv file to postgresql.
it checks all the rows and parses it's values to see what types are there.
and creates a new table with the name of the imported csv file and columns wit the discovered types.

##printlevel
prints all the keys and values (converted to string) from specified leveldb directory

##process_mining
experimental algorithms learned from an [coursera](https://www.coursera.org/learn/process-mining/home/info) course.
programed along the with the running course :P

##tel
minimal golang web app for phone numbers CRUD with [mdl](https://getmdl.io/)
