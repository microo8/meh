package main

import (
	"fmt"
	"log"
	"os"

	"github.com/microo8/meh/cexio"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Llongfile)
	c, err := cexio.Connect(apiKey, apiSecret)
	if err != nil {
		log.Fatalf("error connecting and authentificating (%s)", err)
	}
	defer c.Close()
	go saveOHLCV(c, "ETH", "USD", "1m", "/tmp/cexrate_eth")
	saveOHLCV(c, "BTC", "USD", "1m", "/tmp/cexrate_btc")
}

func saveOHLCV(c *cexio.Client, cc1, cc2, interval, path string) {
	for ohlcv := range c.OHLCV(cc1, cc2, interval) {
		f, err := os.Create(path)
		if err != nil {
			log.Fatalf("cannot open " + path)
		}
		fmt.Fprintf(f, "H:%.2f L:%.2f", ohlcv.High, ohlcv.Low)
		log.Printf("%s:%s - %v", cc1, cc2, ohlcv)
		f.Close()
	}
}
