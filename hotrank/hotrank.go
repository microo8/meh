package hotrank

import (
	"math"
	"sort"
	"time"
)

var t = time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)

//Item - hotrank can sort items with this interface implemented
type Item interface {
	//Ups returns the number of positive upvotes
	Ups() int
	//Downs returns the number negative downvotes
	Downs() int
	//Time returns the creation time of the item
	Time() time.Time
}

type items []Item

func (its items) Len() int           { return len(its) }
func (its items) Swap(i, j int)      { its[i], its[j] = its[j], its[i] }
func (its items) Less(i, j int) bool { return Score(its[i]) < Score(its[j]) }

//Sort sorts item array with the hot rank algorithm
func Sort(i []Item) {
	sort.Sort((items)(i))
}

//Score calculates the actual score of an item
func Score(item Item) float64 {
	s := float64(item.Ups() - item.Downs())
	order := math.Log10(math.Max(math.Abs(s), 1.0))
	seconds := item.Time().Sub(t).Seconds() - 1134028003
	return math.Copysign(1, s)*order + seconds/45000
}
