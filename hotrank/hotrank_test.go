package hotrank

import (
	"fmt"
	"testing"
	"time"
)

type Entry struct {
	ups   int
	downs int
	t     time.Time
}

func (e Entry) Ups() int        { return e.ups }
func (e Entry) Downs() int      { return e.downs }
func (e Entry) Time() time.Time { return e.t }
func (e Entry) String() string {
	return fmt.Sprintf("{Ups: %d, Downs: %d, Time: %s}", e.ups, e.downs, e.t.Format("2006-01-02 15:04:05"))
}

func TestSort(t *testing.T) {
	entries := []Item{
		Entry{100, 0, time.Now()},
		Entry{1000, 10, time.Now().Add(-time.Second * 50)},
		Entry{100, 100, time.Now().Add(-time.Second * 3600)},
		Entry{100, 200, time.Now().Add(-time.Second * 500)},
		Entry{20, 0, time.Now().Add(-time.Second * 300)},
		Entry{1000, 0, time.Now()},
	}
	Sort(entries)
	fmt.Println(entries)
}
