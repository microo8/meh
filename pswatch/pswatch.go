package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net"
	"net/smtp"
	"os"
	"strings"
	"time"

	ps "github.com/mitchellh/go-ps"
)

//Config for the pswatch
type Config struct {
	SMTP            string
	Username        string
	Password        string
	From            string
	To              []string
	Processes       []string
	IntervalMinutes int `json:"interval_minutes"`
}

var config Config

func init() {
	file, err := os.Open("config.json")
	if err != nil {
		fmt.Println("Error reading config file:", err)
		file.Close()
		return
	}
	dec := json.NewDecoder(file)
	err = dec.Decode(&config)
	if err != nil {
		fmt.Println("Error decoding config file:", err)
		file.Close()
		return
	}
	file.Close()
}

func main() {
	for range time.Tick(time.Duration(config.IntervalMinutes) * time.Second) {
		processes, err := ps.Processes()
		if err != nil {
			fmt.Println("Cannot get running processes:", err)
			return
		}
		var notRunning []string
		for _, p := range config.Processes {
			runs := false
			for _, runningProcess := range processes {
				exec := runningProcess.Executable()
				if len(exec) >= len(p) && exec[len(exec)-len(p):] == p {
					runs = true
					break
				}
			}
			if !runs {
				notRunning = append(notRunning, p)
			}
		}
		if len(notRunning) == 0 {
			continue
		}

		// Set up authentication information.
		host, _, _ := net.SplitHostPort(config.SMTP)
		auth := smtp.PlainAuth("", config.Username, config.Password, host)
		// TLS config
		tlsconfig := &tls.Config{
			InsecureSkipVerify: true,
			ServerName:         host,
		}
		conn, err := tls.Dial("tcp", config.SMTP, tlsconfig)
		if err != nil {
			fmt.Println("Cannot connect to smtp:", err)
			return
		}
		c, err := smtp.NewClient(conn, host)
		if err != nil {
			fmt.Println("SMTP client creation error:", err)
			return
		}
		if err = c.Auth(auth); err != nil {
			fmt.Println("SMTP client auth error:", err)
			return
		}
		if err = c.Mail(config.From); err != nil {
			fmt.Println("SMTP client setting From error:", err)
			return
		}
		for _, to := range config.To {
			if err = c.Rcpt(to); err != nil {
				fmt.Println("SMTP client setting From error:", err)
				return
			}
		}
		msg := []byte("To: " + strings.Join(config.To, ", ") + "\r\n" +
			"Subject: PSWatch\r\n" +
			"\r\n" +
			"Not running processes: \r\n" +
			strings.Join(notRunning, ", "))
		w, err := c.Data()
		if err != nil {
			fmt.Println("SMTP client data error:", err)
			return
		}
		_, err = w.Write([]byte(msg))
		if err != nil {
			fmt.Println("SMTP client data write error:", err)
			return
		}
		err = w.Close()
		if err != nil {
			fmt.Println("SMTP client data close error:", err)
			return
		}
		err = c.Quit()
		if err != nil {
			fmt.Println("SMTP client quit error:", err)
			return
		}
		fmt.Println("Email send, not running:", notRunning)
	}
}
