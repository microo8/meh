package main

import (
	"os"
	"syscall"
	"unsafe"

	"github.com/pkg/errors"
	"golang.org/x/sys/windows/registry"
)

//const ...
const (
	pathRegKey       = "Path"
	HWND_BROADCAST   = uintptr(0xffff)
	WM_SETTINGCHANGE = uintptr(0x001A)
)

func getRegEnvValue(key string) (string, error) {
	k, err := registry.OpenKey(registry.LOCAL_MACHINE, `System\CurrentControlSet\Control\Session Manager\Environment`, registry.QUERY_VALUE)
	if err != nil {
		return "", err
	}
	defer k.Close()
	s, _, err := k.GetStringValue(key)
	return s, err
}

func saveRegEnvValue(key string, value string) error {
	k, err := registry.OpenKey(registry.LOCAL_MACHINE, `System\CurrentControlSet\Control\Session Manager\Environment`, registry.SET_VALUE)
	if err != nil {
		return err
	}
	defer k.Close()
	return k.SetStringValue(key, value)
}

//AppendPath ...
func AppendPath(newPath string) error {
	path, err := getRegEnvValue(pathRegKey)
	if err != nil {
		return errors.Wrap(err, "getting PATH value")
	}
	if err := saveRegEnvValue(pathRegKey, path+string(os.PathListSeparator)+newPath); err != nil {
		return errors.Wrap(err, "setting PATH value")
	}
	syscall.NewLazyDLL("user32.dll").NewProc("SendMessageW").Call(HWND_BROADCAST, WM_SETTINGCHANGE, 0, uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr("ENVIRONMENT"))))
	return nil
}
