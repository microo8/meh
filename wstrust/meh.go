package main

import (
	"log"

	wsfed "github.com/ma314smith/go-wsfed"
)

var (
	sso *wsfed.WSFed
)

const (
	stsURL      = "https://authws.vyvoj.upvs.globaltel.sk/sts/wss11x509"
	metadataURL = "https://auth.vyvoj.upvs.globaltel.sk/oamfed/idp/metadata?version=saml20"
)

func init() {
	sso = wsfed.New(&wsfed.Config{
		IDPEndpoint: stsURL,
		// set the metatdata url for the IDP (alternatively set the IDPEndpoint)
		MetadataURL: metadataURL,
		// trust the certs in the metadata (alternatively set the TrustedCerts)
		MetadataCertsAreTrusted: true,
		// poll the metadata once a week to check for any new certificates.
		// the default is 0 seconds, which never refreshes after the initial poll
		MetadataRefreshIntervalSeconds: 7 * 24 * 60 * 60,
		// set your realm
		Realm: "http://realm.example.com",
	})
}

func main() {
	rp := sso.GetDefaultRequestParameters()
	url, err := sso.GetRequestURL(rp)
	if err != nil {
		log.Fatalln(err)
		return
	}
	log.Println(url)
}
