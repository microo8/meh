package main

import (
	"flag"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
)

func main() {
	flag.Parse()

	//set package path
	packagePath := "."
	if len(flag.Args()) == 1 {
		packagePath = flag.Arg(0)
	}

	fset := token.NewFileSet()
	f, err := parser.ParseDir(fset, packagePath, nil, parser.ParseComments)
	if err != nil {
		panic(err)
	}
	if len(f) > 1 {
		fmt.Println("More than one package in", packagePath)
		return
	}

	for packageName, packageAst := range f {
		fmt.Println("Package", packageName+":")
		for fileName, file := range packageAst.Files {
			fmt.Println("File", fileName+":")
			ast.Print(fset, file)
		}
	}
}
