package main

import (
	"fmt"
	"math"
)

//HSV ...
type HSV struct {
	H, S, V float64
}

//RGB ...
type RGB struct {
	R, G, B float64
}

//HEX ...
func (c *RGB) HEX() string {
	return fmt.Sprintf("#%02x%02x%02x", int(c.R), int(c.G), int(c.B))
}

//HSV ...
func (c *RGB) HSV() *HSV {
	r := (c.R / 255) //RGB from 0 to 255
	g := (c.G / 255)
	b := (c.B / 255)

	min := math.Min(r, math.Min(g, b)) //Min. value of RGB
	max := math.Max(r, math.Max(g, b)) //Max. value of RGB
	del := max - min                   //Delta RGB value

	v := max

	var h, s float64

	if del == 0 { //This is a gray, no chroma...
		h = 0 //HSV results from 0 to 1
		s = 0
	} else { //Chromatic data...
		s = del / max

		delR := (((max - r) / 6) + (del / 2)) / del
		delG := (((max - g) / 6) + (del / 2)) / del
		delB := (((max - b) / 6) + (del / 2)) / del

		if r == max {
			h = delB - delG
		} else if g == max {
			h = (1 / 3) + delR - delB
		} else if b == max {
			h = (2 / 3) + delG - delR
		}

		if h < 0 {
			h++
		}
		if h > 1 {
			h--
		}
	}
	hsv := &HSV{h, s, v}
	return hsv
}

//RGB ...
func (c *HSV) RGB() *RGB {
	var r, g, b float64
	if c.S == 0 { //HSV from 0 to 1
		r = c.V * 255
		g = c.V * 255
		b = c.V * 255
	} else {
		h := c.H * 6
		if h == 6 {
			h = 0
		} //H must be < 1
		i := math.Floor(h) //Or ... var_i = floor( var_h )
		v1 := c.V * (1 - c.S)
		v2 := c.V * (1 - c.S*(h-i))
		v3 := c.V * (1 - c.S*(1-(h-i)))

		if i == 0 {
			r = c.V
			g = v3
			b = v1
		} else if i == 1 {
			r = v2
			g = c.V
			b = v1
		} else if i == 2 {
			r = v1
			g = c.V
			b = v3
		} else if i == 3 {
			r = v1
			g = v2
			b = c.V
		} else if i == 4 {
			r = v3
			g = v1
			b = c.V
		} else {
			r = c.V
			g = v1
			b = v2
		}

		r = r * 255 //RGB results from 0 to 255
		g = g * 255
		b = b * 255
	}
	rgb := &RGB{r, g, b}
	return rgb

}

func hueTorgb(v1, v2, vH float64) float64 { //Function Hue_2_RGB
	if vH < 0 {
		vH++
	}
	if vH > 1 {
		vH--
	}
	if (6 * vH) < 1 {
		return (v1 + (v2-v1)*6*vH)
	}
	if (2 * vH) < 1 {
		return v2
	}
	if (3 * vH) < 2 {
		return (v1 + (v2-v1)*((2/3)-vH)*6)
	}
	return v1
}
