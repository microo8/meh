# coinmarketcap for i3bar

Example usage in `~/.config/i3/i3blocks.conf`:

```ini
[bitcoin]
label=฿
command=cmc -s bitcoin
markup=pango
interval=5

[iota]
label=IOTA
command=cmc -s iota
markup=pango
interval=5

[ada]
label=ADA
command=cmc -s cardano
markup=pango
interval=5
```
