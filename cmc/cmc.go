/*
coinmarketcap command to use in i3blocks

eg.

[cmc]
command=cmc -s iota
markup=pango
interval=5
*/

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strconv"
)

const cmcURL = "https://api.coinmarketcap.com/v1/ticker/"

var (
	symbol = flag.String("s", "bitcoin", "the cryptocurrency name")
)

type ticker struct {
	PriceUSD         string `json:"price_usd"`
	PriceBTC         string `json:"price_btc"`
	PercentChange1h  string `json:"percent_change_1h"`
	PercentChange24h string `json:"percent_change_24h"`
}

func getTicker() (*ticker, error) {
	resp, err := http.Get(cmcURL + *symbol)
	if err != nil {
		return nil, err
	}
	t := []*ticker{new(ticker)}
	if err = json.NewDecoder(resp.Body).Decode(&t); err != nil {
		return nil, err
	}
	return t[0], nil
}

func percentageToColor(p float64) *HSV {
	if p > 0 {
		return &HSV{H: (50*(1-p/100) + 100) / 360, S: 1, V: 1}
	}
	return &HSV{H: (20 * (1 - p/100)) / 360, S: 1, V: 1}
}

func main() {
	flag.Parse()
	t, err := getTicker()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error getting ticker: %s\n", err)
		return
	}
	changeValue1h, err := strconv.ParseFloat(t.PercentChange1h, 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error converting percent change: %s\n", err)
		return
	}
	changeValue24h, err := strconv.ParseFloat(t.PercentChange24h, 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error converting percent change: %s\n", err)
		return
	}
	priceUSD, err := strconv.ParseFloat(t.PriceUSD, 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error converting price_usd: %s\n", err)
		return
	}
	priceBTC, err := strconv.ParseFloat(t.PriceBTC, 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error converting price_btc: %s\n", err)
		return
	}
	color := percentageToColor(changeValue24h).RGB().HEX()
	direction := "▾"
	if changeValue1h >= 0 {
		direction = "▴"
	}
	if *symbol == "bitcoin" {
		fmt.Printf(`<span color="%s">%s%.2f%% $%.6f</span>`, color, direction, changeValue24h, priceUSD)
		return
	}
	fmt.Printf(`<span color="%s">%s%.2f%% $%.6f ฿%.6f</span>`, color, direction, changeValue24h, priceUSD, priceBTC)
}
