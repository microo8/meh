package jointest

import (
	"strings"
	"testing"

	"github.com/gopherjs/gopherjs/js"
)

func Join(a []string, sep string) string {
	return js.InternalObject(a).Get("$array").Call("join", js.InternalObject(sep)).String()
}

var faces = "☺☻☹"

var tests = [][]string{
	[]string{"a", "bcd"},
	[]string{"abcd"},
	[]string{"a", "b", "c", "d"},
	[]string{"1,", "2,", "3,", "4"},
	[]string{"1...", ".2...", ".3...", ".4"},
	[]string{"☺☻☹", ""},
	[]string{faces},
	[]string{"☺", "☻", "☹"},
}
var testArray = []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z"}

func TestEqual(t *testing.T) {
	for _, test := range tests {
		want := strings.Join(test, ",")
		got := Join(test, ",")
		if want != got {
			t.Errorf("not equal; want: %s; got: %s", want, got)
		}
	}
}

func BenchmarkNative(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Join(testArray, ",")
	}
}

func BenchmarkStrings(b *testing.B) {
	for n := 0; n < b.N; n++ {
		strings.Join(testArray, ",")
	}
}
