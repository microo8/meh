package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/julienschmidt/httprouter"
	rpio "github.com/stianeikeland/go-rpio"
)

var (
	port   = flag.Int("p", 8080, "port")
	static = flag.String("s", ".", "path to the static content")
)

func main() {
	flag.Parse()

	// Open and map memory to access gpio, check for errors
	if err := rpio.Open(); err != nil {
		log.Println(err)
		os.Exit(1)
	}

	// Unmap gpio memory when done
	defer rpio.Close()

	router := httprouter.New()
	router.GET("/pin/:pin/:action", handler)
	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir(*static)))
	mux.Handle("/pin/", router)
	http.ListenAndServe(fmt.Sprintf(":%d", *port), mux)
}

const (
	usage = `route must be '/pin_number/action'
eg.
/10/high - sets pin 10 to high
/17/output - sets pin 17 to output mode

available actions:
output - sets pin to output mode
input - sets pin to input mode
high - sets pin to high
low - sets pin to low
toggle - toggles pins output
read - reads from pin and returns state (low/high)
`
)

func handler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	pinNum, err := strconv.Atoi(ps.ByName("pin"))
	if err != nil {
		http.Error(w, "can't get pin number\n"+usage, 400)
		return
	}
	pin := rpio.Pin(pinNum)
	defer pinError(w)
	switch ps.ByName("action") {
	case "output":
		pin.Output()
	case "input":
		pin.Input()
	case "high":
		pin.High()
	case "low":
		pin.Low()
	case "toggle":
		pin.Toggle()
	case "read":
		res := pin.Read()
		if res == rpio.High {
			w.Write([]byte("high"))
		} else {
			w.Write([]byte("low"))
		}
	default:
		http.Error(w, fmt.Sprintf("not valid action: %s%s\n", ps.ByName("action"), usage), 400)
	}
}

func pinError(w http.ResponseWriter) {
	r := recover()
	if r == nil {
		return
	}
	http.Error(w, fmt.Sprintf("not valid pin number: %s", r), 400)
}
