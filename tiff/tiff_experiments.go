package main

import (
	"image"
	_ "image/jpeg"
	"os"
	"path/filepath"

	"golang.org/x/image/tiff"
)

func main() {
	var imgs []image.Image
	files, err := filepath.Glob("*.jpg")
	if err != nil {
		panic(err)
	}
	for _, f := range files {
		imgf, err := os.Open(f)
		if err != nil {
			panic(err)
		}
		img, _, err := image.Decode(imgf)
		if err != nil {
			panic(err)
		}
		imgf.Close()
		imgs = append(imgs, img)
	}
	out, err := os.Create("out.tiff")
	if err != nil {
		panic(err)
	}
	defer out.Close()
	for _, img := range imgs {
		err = tiff.Encode(out, img, nil)
		if err != nil {
			panic(err)
		}
	}
}
