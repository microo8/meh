package googletoken

import (
	"bytes"
	"crypto/rsa"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"net/http"
	"net/url"
	"strings"
)

var (
	cert *rsa.PublicKey
)

func init() {
	res, err := http.Get("https://www.googleapis.com/oauth2/v2/certs")
	if err != nil {
		log.Fatal(err)
		return
	}
	defer res.Body.Close()
	goCertificate := make(map[string][]map[string]string)
	err = json.NewDecoder(res.Body).Decode(&goCertificate)
	if err != nil {
		log.Fatal(err)
		return
	}
	certs := goCertificate["keys"]
	//get n
	n := certs[1]["n"]
	decN, err := base64.URLEncoding.DecodeString(n)
	if err != nil {
		log.Println(err)
		return
	}
	bn := big.NewInt(0).SetBytes(decN)
	//get e
	e := certs[1]["e"]
	decE, err := base64.URLEncoding.DecodeString(e)
	if err != nil {
		log.Println(err)
		return
	}
	var eBytes []byte
	if len(decE) < 8 {
		eBytes = make([]byte, 8-len(decE), 8)
		eBytes = append(eBytes, decE...)
	} else {
		eBytes = decE
	}
	eReader := bytes.NewReader(eBytes)
	var be uint64
	err = binary.Read(eReader, binary.BigEndian, &be)
	if err != nil {
		log.Println(err)
		return
	}
	cert = &rsa.PublicKey{N: bn, E: int(be)}
}

//TokenInfo is the json document encoded in
type TokenInfo struct {
	ISS string
	SUB string
	AZP string
	AUD string
	IAT string
	EXP string

	// These seven fields are only included when the user has granted the "profile" and
	// "email" OAuth scopes to the application.
	Email         string
	EmailVerified bool `json:"email_verified"`
	Name          string
	Picture       url.URL
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Locale        string
}

//Verify and decode the id_token
func Verify(idToken string) (*TokenInfo, error) {
	splitIDToken := strings.Split(idToken, ".")
	if len(splitIDToken) != 3 {
		return nil, fmt.Errorf("Bad id_token encoding")
	}
	messageToSign := splitIDToken[0] + "." + splitIDToken[1]
	signature := splitIDToken[2]
	//Parse token.
	payloadBytes, err := base64.URLEncoding.DecodeString(splitIDToken[1])
	if err != nil {
		return nil, err
	}
	var tok TokenInfo
	err = json.Unmarshal(payloadBytes, &tok)
	if err != nil {
		return nil, fmt.Errorf("Can't parse token: %s", err)
	}
	/*
	   # Verify that the signature matches the message.
	   _verify_signature(message_to_sign, signature, certs.values())

	   # Verify the issued at and created times in the payload.
	   _verify_time_range(payload_dict)

	   # Check audience.
	   _check_audience(payload_dict, audience)

	   return payload_dict
	*/
}
