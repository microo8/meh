// +build windows

package main

import (
	"log"
	"syscall"
)

const (
	ATTACH_PARENT_PROCESS = ^uint32(0) // (DWORD)-1
)

var (
	modkernel32       = syscall.NewLazyDLL("kernel32.dll")
	procAttachConsole = modkernel32.NewProc("AttachConsole")
)

func attachConsole(dwParentProcess uint32) bool {
	r0, _, _ := syscall.Syscall(procAttachConsole.Addr(), 1, uintptr(dwParentProcess), 0, 0)
	return bool(r0 != 0)
}
func init() {
	if ok := attachConsole(ATTACH_PARENT_PROCESS); !ok {
		log.Println("not attached")
	}
}
