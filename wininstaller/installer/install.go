package main

import (
	"log"
	"os"
	"time"

	"golang.org/x/sys/windows/registry"
)

func main() {
	f, err := os.Create("log")
	if err != nil {
		log.Panic(err)
	}
	defer f.Close()
	log.SetOutput(f)
	os.MkdirAll(`C:\Program Files\nws_test`, 0777)
	f, err = os.Create(`C:\Program Files\nws_test\uninstall.exe`)
	if err != nil {
		log.Panic(err)
	}
	f.Close()
	k, _, err := registry.CreateKey(
		registry.LOCAL_MACHINE,
		`SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\nws_test`,
		registry.ALL_ACCESS,
	)
	if err != nil {
		log.Panic(err)
	}
	defer k.Close()
	if err := k.SetStringValue("DisplayName", "nws_test 0.1"); err != nil {
		log.Panic(err)
	}
	log.Println("written DisplayName")
	if err := k.SetStringValue("UninstallString", `C:\Program Files\nws_test\uninstall.exe`); err != nil {
		log.Panic(err)
	}
	log.Println("written UninstallString")
	time.Sleep(time.Second * 5)
}
