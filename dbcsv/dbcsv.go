//tool to import csv file to postgresql or mysql
package main

import (
	"bytes"
	"database/sql"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
	"unicode"

	"github.com/cheggaaa/pb"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
)

var (
	dbConn = flag.String("db", "postgres://postgres:password@localhost:5432/postgres", `database connection string
	example:
	postgres://postgres:password@localhost:5432/postgres
	mysql://root:password@tcp(localhost:3306)/test?charset=utf8
	`)
	filename    = flag.String("f", "data.csv", "csv file path")
	verbose     = flag.Bool("v", false, "prints out the values sended to the table")
	unaccentMap = make(map[rune]rune)
)

func init() {
	from := []rune("¹²³áàâãäåāăąÀÁÂÃÄÅĀĂĄÆćčç©ĆČÇĐÐĎďèéêёëēĕėęěÈÊËЁĒĔĖĘĚ€ğĞıìíîïìĩīĭÌÍÎÏЇÌĨĪĬłŁĺĹľĽńňñŃŇÑòóôõöōŏőøÒÓÔÕÖŌŎŐØŒř®ŘŕšşșßŠŞȘťŤùúûüũūŭůűÙÚÛÜŨŪŬŮýÿÝŸžżźŽŻŹ")
	to := []rune("123aaaaaaaaaaaaaaaaaaacccccccddddeeeeeeeeeeeeeeeeeeeeggiiiiiiiiiiiiiiiiiillllllnnnnnnooooooooooooooooooorrrrsssssssttuuuuuuuuűuuuuuuuuyyyyzzzzzz")
	for i := range from {
		unaccentMap[from[i]] = to[i]
	}
}

func csvReader(filename string) (chan []string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	reader := csv.NewReader(file)
	rows := make(chan []string, 128)
	go func() {
		for {
			row, err := reader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				fmt.Println("error reading file:", err)
				break
			}
			rows <- row
		}
		close(rows)
	}()
	return rows, nil
}

func isInt(v string) bool {
	_, err := strconv.Atoi(v)
	return err == nil
}
func isFloat(v string) bool {
	_, err := strconv.ParseFloat(v, 64)
	return err == nil
}
func isDate(v string) bool {
	_, err := time.Parse("1/2/2006", v)
	if err == nil {
		return true
	}
	_, err = time.Parse("2/1/2006", v)
	if err == nil {
		return true
	}
	_, err = time.Parse("2006-01-02", v)
	return err == nil
}

func toDate(v string) string {
	t, err := time.Parse("1/2/2006", v)
	if err == nil {
		return t.Format("2006-01-02")
	}
	t, err = time.Parse("2/1/2006", v)
	if err == nil {
		return t.Format("2006-01-02")
	}
	t, err = time.Parse("2006-01-02", v)
	if err == nil {
		return t.Format("2006-01-02")
	}
	return ""
}

func lowerUnaccent(v string) string {
	var ret bytes.Buffer
	for _, c := range strings.ToLower(v) {
		if unicode.IsLetter(c) {
			if r, ok := unaccentMap[c]; ok {
				ret.WriteRune(r)
			} else {
				ret.WriteRune(c)
			}
		}
	}
	return ret.String()
}

func main() {
	flag.Parse()

	rows, err := csvReader(*filename)
	if err != nil {
		fmt.Println(err)
		return
	}
	cols := <-rows

	colTypes := make([]string, len(cols))
	for i := range colTypes {
		colTypes[i] = "text"
	}

	isAllEmpty := make([]bool, len(cols))
	for i := range isAllEmpty {
		isAllEmpty[i] = true
	}
	isAllInt := make([]bool, len(cols))
	for i := range isAllInt {
		isAllInt[i] = true
	}
	isAllFloat := make([]bool, len(cols))
	for i := range isAllFloat {
		isAllFloat[i] = true
	}
	isAllDate := make([]bool, len(cols))
	for i := range isAllDate {
		isAllDate[i] = true
	}

	var count int
	for row := range rows {
		count++
		for i, c := range row {
			c = strings.TrimSpace(c)
			isAllEmpty[i] = isAllEmpty[i] && c == ""
			isAllInt[i] = isAllInt[i] && (c == "" || isInt(c))
			isAllFloat[i] = isAllFloat[i] && (c == "" || isFloat(c))
			isAllDate[i] = isAllDate[i] && (c == "" || isDate(c))
		}
	}

	for i := range colTypes {
		if isAllEmpty[i] {
			colTypes[i] = "text"
		} else if isAllInt[i] {
			colTypes[i] = "bigint"
		} else if isAllFloat[i] {
			colTypes[i] = "double precision"
		} else if isAllDate[i] {
			colTypes[i] = "date"
		}
	}

	dbType := (*dbConn)[:strings.Index(*dbConn, "://")]
	c := '"'
	if dbType == "mysql" {
		c = '`'
		*dbConn = (*dbConn)[len(dbType)+3:]
	}
	db, err := sql.Open(dbType, *dbConn)
	if err != nil {
		fmt.Println("connection error:", err)
		return
	}
	defer db.Close()
	err = db.Ping()
	if err != nil {
		fmt.Println("connection error:", err)
		return
	}

	tableName := path.Base(*filename)
	tableName = tableName[:len(tableName)-4]

	_, err = db.Exec(fmt.Sprintf(`drop table if exists %c%s%c`, c, tableName, c))
	if err != nil {
		fmt.Println("cannot drop table error:", err)
		return
	}

	var colDefs bytes.Buffer
	for i, col := range cols {
		if strings.TrimSpace(lowerUnaccent(col)) == "" {
			fmt.Printf("column number %d has no name\n", i+1)
			return
		}
		colDefs.WriteString(fmt.Sprintf(", %c%s%c %s", c, lowerUnaccent(col), c, colTypes[i]))
	}

	if _, err = db.Exec(fmt.Sprintf(`create table %c%s%c (%s)`, c, tableName, c, colDefs.String()[1:])); err != nil {
		fmt.Println("cannot create table error:", err)
		return
	}

	var params bytes.Buffer
	for i := range cols {
		if dbType == "mysql" {
			params.WriteString(",?")
		} else {
			params.WriteString(fmt.Sprintf(",$%d", i+1))
		}
	}

	insert, err := db.Prepare(fmt.Sprintf("insert into %s values (%s)", tableName, params.String()[1:]))
	if err != nil {
		fmt.Println("insert prepare error:", err)
		return
	}
	defer insert.Close()

	tx, err := db.Begin()
	if err != nil {
		fmt.Println("cannot start transaction:", err)
		return
	}
	txInsert := tx.Stmt(insert)

	rows, err = csvReader(*filename)
	if err != nil {
		fmt.Println(err)
		return
	}
	<-rows
	irow := make([]interface{}, len(cols))
	bar := pb.StartNew(count)
	for row := range rows {
		for i, r := range row {
			r = strings.TrimSpace(r)
			if r == "" {
				irow[i] = nil
			} else if colTypes[i] == "date" {
				irow[i] = toDate(r)
			} else {
				irow[i] = r
			}
		}
		if *verbose {
			fmt.Println(irow)
		}
		if _, err = txInsert.Exec(irow...); err != nil {
			fmt.Println("insert error:", row, err)
			return
		}
		bar.Increment()
	}
	err = tx.Commit()
	if err != nil {
		fmt.Println("commit error:", err)
		return
	}
	bar.FinishPrint("Done.")
}
