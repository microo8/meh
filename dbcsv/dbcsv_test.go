package main

import "testing"

func TestIsDate(t *testing.T) {
	data := []struct {
		d string
		b bool
	}{
		{"2016-05-31", true},
		{"05/31/2016", true},
		{"31/01/2016", true},
		{"", false},
	}
	for i, d := range data {
		if isDate(d.d) != d.b {
			t.Errorf("Error %d: %v", i, d)
		}
	}
}

func benchLowerUnaccent(input string, f func(string) string, b *testing.B) {
	for n := 0; n < b.N; n++ {
		f(input)
	}

}

func BenchmarkLowerUnaccent21(b *testing.B) {
	benchLowerUnaccent("íánfárenfáíéewhfné áýfáédsafaéíshíhf aséhísafhaísfh", lowerUnaccent, b)
}

func BenchmarkLowerUnaccent22(b *testing.B) {
	benchLowerUnaccent("ínasdfáahfcéaíáshcníšqéfjčšľf=íáfíšfh=oijsafí=ádsafh=š íáfhí=awhfsadnfasfníýčšqéhfpjnoasdfhasí= sadáf jdsaf ís=fhsdáfhj í=sadfédsfj dsaéfáj šľr", lowerUnaccent, b)
}

func BenchmarkLowerUnaccent23(b *testing.B) {
	benchLowerUnaccent("ínasdfáahfcéaíáshcníšqéfjčšľf=íáfíšfh=oijsafí=ádsafh=š íáfhí=awhfsadnfasfníýčšqéhfpjnoasdfhasí= sadáf jdsaf ís=fhsdáfhj í=sadfédsfj dsaéfáj šľr safbo safb s9d0f98fh=íyhídsafsadíá uýf=dsaíy dí=f é19s1d a*f 4rsa f+6+čš", lowerUnaccent, b)
}
