package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

func downloadProfileImg(user *twitter.User) string {
	resp, err := http.Get(user.ProfileImageURL)
	if err != nil {
		return ""
	}
	f, err := ioutil.TempFile("/tmp", "profileImage")
	if err != nil {
		return ""
	}
	defer f.Close()
	io.Copy(f, resp.Body)
	return f.Name()
}

func main() {
	config := oauth1.NewConfig("dXaPd76kTgiO71Mm1B18UBYY1", "hHaaGSbqqHUFiDnJPZHCe7yg97m0459kMxC13zKvYvGu6AiRXo")
	token := oauth1.NewToken("22891228-390V2sIPMpPgEcOSd89htjXWkkMPcfRFW1j5dDZPQ", "b7g2gbbTKG8zMMbPfj5l4domUthqWYmMNO95UfFJuhMnb")
	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)
	var uids []string
	for _, username := range os.Args[1:] {
		user, _, err := client.Users.Show(&twitter.UserShowParams{
			ScreenName: username,
		})
		if err != nil {
			panic(err)
		}
		uids = append(uids, user.IDStr)
	}
	params := &twitter.StreamFilterParams{
		Follow: uids,
	}
	stream, err := client.Streams.Filter(params)
	if err != nil {
		panic(err)
	}
	for message := range stream.Messages {
		tweet, ok := message.(*twitter.Tweet)
		if !ok {
			continue
		}
		if tweet.RetweetedStatus != nil {
			continue
		}
		profileImage := downloadProfileImg(tweet.User)
		cmd := exec.Command("notify-send", "--icon="+profileImage, tweet.User.ScreenName, tweet.Text)
		err := cmd.Start()
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
		}
	}
}
