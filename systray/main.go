package main

/*
#cgo LDFLAGS: -lX11
#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>

#define SYSTEM_TRAY_REQUEST_DOCK 0

Display *display;
int screen;
Window root;
Atom wm_delete_window;
struct list_container_s *list_container;

void init(void)
{
	display = XOpenDisplay(0);
	if (!display) printf("display");
	screen = DefaultScreen(display);
	root = RootWindow(display, screen);
	wm_delete_window = XInternAtom(display, "WM_DELETE_WINDOW", 0);
	//_net_wm_state = XInternAtom(display, "_NET_WM_STATE", 0);
	//_net_wm_state_hidden = XInternAtom(display, "_NET_WM_STATE_ADD", 0);
	list_container = 0; // init list.
}

void tray_init()
{
	XEvent event;
	Window systray;
	Window icon;
	Atom tray_atom;

	tray_atom = XInternAtom(display, "_NET_SYSTEM_TRAY_S0", False);
	if (!tray_atom)	return;
	systray = XGetSelectionOwner(display, tray_atom);
	if (!systray) return;

	//icon = gui->create_window();

	memset(&event, 0, sizeof(event));
	event.xclient.type = ClientMessage;
	event.xclient.window = systray;
	event.xclient.message_type = 0;
	event.xclient.format = 32;
	event.xclient.data.l[0] = 0;
	event.xclient.data.l[1] = SYSTEM_TRAY_REQUEST_DOCK;
	event.xclient.data.l[2] = icon;

	//XSendEvent(display, systray, False, NoEventMask, False);
	XSync(display, False);
}

static void event_handler(void)
{
	XEvent event;

	for (;;) {
		XNextEvent(display, &event);
		switch (event.type) {
			case ClientMessage:
			if ((Atom) event.xclient.data.l[0] == wm_delete_window) {
				XWithdrawWindow(display, event.xclient.window, screen);
			}
			break;
			//case MapNotify:
			//case GraphicsExpose:
			//case Expose:
			//	window_expose(&event);
			//break;
		}
	}
}
*/
import "C"

func main() {
	C.init()
	C.tray_init()
	C.event_handler()
}
