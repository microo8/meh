package service2

import (
	"bufio"
	"os"
)

//Service is the Service1 with its public API
type Service struct {
	Endpoint1 chan *Req1
}

//NewService returns a new Service
func NewService() *Service {
	return &Service{
		Endpoint1: make(chan *Req1),
	}
}

//Run spins up an go routine in which the Service processes its requests
func (s *Service) Run() {
	go func() {
		for {
			select {
			case req1 := <-s.Endpoint1:
				go s.ProcessReq1(req1)
			}
		}
	}()
}

//ProcessReq1 processes request Req1 and returns an response
func (s *Service) ProcessReq1(req1 *Req1) {
	err := s.addLine(req1.Query)
	if err != nil {
		panic(err)
	}
	lines, err := s.readLines()
	if err != nil {
		panic(err)
	}
	req1.Resp <- &Resp1{
		Result: lines,
	}
}

func (s *Service) addLine(name string) error {
	f, err := os.OpenFile("db.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer f.Close()
	if _, err := f.WriteString(name + "\n"); err != nil {
		return err
	}
	return nil
}

func (s *Service) readLines() ([]string, error) {
	f, err := os.Open("db.txt")
	if err != nil {
		return nil, err
	}
	var lines []string
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

//Req1 used to send request to the Endpoint1 of Service
type Req1 struct {
	Query string
	Resp  chan *Resp1
}

//Resp1 used as an response from the Endpoint1 of Service
type Resp1 struct {
	Result []string
}
