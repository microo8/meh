package main

import (
	"net/http"
	"strings"

	"github.com/microo8/meh/services/service1"
	"github.com/microo8/meh/services/service2"
)

var (
	s1 = service1.NewService()
	s2 = service2.NewService()
)

func hello(w http.ResponseWriter, r *http.Request) {
	resp1chan := make(chan *service1.Resp1)
	s1.Endpoint1 <- &service1.Req1{
		Query: r.URL.Path[7:],
		Resp:  resp1chan,
	}
	resp1 := <-resp1chan
	w.Write([]byte(resp1.Result))
}

func list(w http.ResponseWriter, r *http.Request) {
	resp1chan := make(chan *service2.Resp1)
	s2.Endpoint1 <- &service2.Req1{
		Query: r.URL.Path,
		Resp:  resp1chan,
	}
	resp1 := <-resp1chan
	w.Write([]byte(strings.Join(resp1.Result, "<br/>")))
}

func main() {
	s1.Run()
	s2.Run()
	http.HandleFunc("/hello/", hello)
	http.HandleFunc("/list/", list)
	http.ListenAndServe(":8080", nil)
}
