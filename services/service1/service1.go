package service1

//Service is the Service1 with its public API
type Service struct {
	Endpoint1 chan *Req1
}

//NewService returns a new Service
func NewService() *Service {
	return &Service{
		Endpoint1: make(chan *Req1),
	}
}

//Run spins up an go routine in which the Service processes its requests
func (s *Service) Run() {
	go func() {
		for {
			select {
			case req1 := <-s.Endpoint1:
				go s.ProcessReq1(req1)
			}
		}
	}()
}

//ProcessReq1 processes request Req1 and returns an response
func (s *Service) ProcessReq1(req1 *Req1) {
	req1.Resp <- &Resp1{
		Result: "Hello " + req1.Query,
	}
}

//Req1 used to send request to the Endpoint1 of Service
type Req1 struct {
	Query string
	Resp  chan *Resp1
}

//Resp1 used as an response from the Endpoint1 of Service
type Resp1 struct {
	Result string
}
