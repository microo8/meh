package main

//go:generate go-bindata -pkg templ -o templates/bindata.go templates/
//go:generate go-bindata-assetfs assets

import (
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/user"
	"path"
	"sort"
	"strconv"
	"strings"

	tiedot "github.com/HouzuoGuo/tiedot/db"
	"github.com/julienschmidt/httprouter"
	"github.com/microo8/meh/tel/templates"
	"github.com/pkg/browser"
	"github.com/tealeg/xlsx"
)

var (
	router    = httprouter.New()
	port      = flag.Int("port", 23456, "Port na ktorom beží http server")
	templates = template.New("")
	db        *tiedot.DB
	persons   *tiedot.Col
)

//SortableDocuments implements the sort.Interface and sorts documents by an key
type SortableDocuments struct {
	docs []map[string]interface{}
	key  string
}

// Len is the number of elements in the collection.
func (d SortableDocuments) Len() int {
	return len(d.docs)
}

//Less reports whether the element with
//index i should sort before the element with index j.
func (d SortableDocuments) Less(i, j int) bool {
	return d.docs[i][d.key].(string) < d.docs[j][d.key].(string)
}

//Swap swaps the elements with indexes i and j.
func (d SortableDocuments) Swap(i, j int) {
	doc := d.docs[i]
	d.docs[i] = d.docs[j]
	d.docs[j] = doc
}

func lowerUnaccent(v string) (ret string) {
	from := []rune("¹²³áàâãäåāăąÀÁÂÃÄÅĀĂĄÆćčç©ĆČÇĐÐĎďèéêёëēĕėęěÈÊËЁĒĔĖĘĚ€ğĞıìíîïìĩīĭÌÍÎÏЇÌĨĪĬłŁĺĹľĽńňñŃŇÑòóôõöōŏőøÒÓÔÕÖŌŎŐØŒř®ŘŕšşșßŠŞȘťŤùúûüũūŭůűÙÚÛÜŨŪŬŮýÿÝŸžżźŽŻŹ")
	to := []rune("123aaaaaaaaaaaaaaaaaaacccccccddddeeeeeeeeeeeeeeeeeeeeggiiiiiiiiiiiiiiiiiillllllnnnnnnooooooooooooooooooorrrrsssssssttuuuuuuuuűuuuuuuuuyyyyzzzzzz")
	dict := make(map[rune]rune)
	for i := range from {
		dict[from[i]] = to[i]
	}
	for _, c := range strings.ToLower(v) {
		if r, ok := dict[c]; ok {
			ret += string(r)
		} else {
			ret += string(c)
		}
	}
	return ret
}

func init() {
	//database
	usr, err := user.Current()
	if err != nil {
		log.Panicln(err)
	}
	dbPath := path.Join(usr.HomeDir, ".tel")
	if _, err = os.Stat(dbPath); err != nil && os.IsNotExist(err) {
		os.Mkdir(dbPath, 0777)
	}
	db, err = tiedot.OpenDB(dbPath)
	if err != nil {
		log.Panicln(err)
	}
	persons = db.Use("persons")
	if persons == nil {
		err = db.Create("persons")
		if err != nil {
			log.Panicln(err)
		}
		persons = db.Use("persons")
	}
	/*
		query, _ = db.Prepare(`select * from person order by name`)
		count, _ = db.Prepare(`select count(*) from person`)
		insert, _ = db.Prepare(`insert into person values ($1,$2,$3)`)
		update, _ = db.Prepare(`update person set name=$2, phone=$3 where id=$1`)
		delete, _ = db.Prepare(`delete from person where id=$1`)
	*/

	//templates
	for _, templatePath := range templ.AssetNames() {
		bytes, err := templ.Asset(templatePath)
		if err != nil {
			log.Panicf("Unable to parse: path=%s, err=%s", templatePath, err)
		}
		templates.New(path.Base(templatePath)).Parse(string(bytes))
	}

	//http routes
	router.ServeFiles("/assets/*filepath", assetFS())
	router.GET("/", index)
	router.POST("/person", insertPerson)
	router.PUT("/person", updatePerson)
	router.DELETE("/person/:id", deletePerson)
	router.GET("/search/*query", search)
	router.POST("/import", importfile)
	router.GET("/exit", func(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
		log.Fatal("exit")
	})
}

//REST
func insertPerson(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	var person map[string]interface{}
	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(&person)
	if err != nil {
		log.Panicln("POST decode", err)
	}

	_, err = persons.Insert(person)
	if err != nil {
		log.Panicf("insert error %s", err)
	}
}

func updatePerson(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	var person map[string]interface{}
	err := json.NewDecoder(req.Body).Decode(&person)
	if err != nil {
		log.Panicln("PUT decode", err)
	}
	id, ok := person["id"]
	if !ok {
		log.Panicf("id not in document %s", person)
	}
	delete(person, "id")
	idInt, err := strconv.Atoi(id.(string))
	if err != nil {
		log.Panicln("conversion error", err)
	}
	err = persons.Update(idInt, person)
	if err != nil {
		log.Panicf("update error %s", err)
	}
}

func deletePerson(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("id"))
	if err != nil {
		log.Panic(err)
	}
	err = persons.Delete(id)
	if err != nil {
		log.Panicf("delete error %s", err)
	}
}

func search(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	s := lowerUnaccent(ps.ByName("query")[1:])
	var data SortableDocuments
	data.key = "name"
	persons.ForEachDoc(func(id int, docContent []byte) bool {
		var person map[string]interface{}
		err := json.Unmarshal(docContent, &person)
		if err != nil {
			log.Panicln("in search: cannot deserialize", err)
		}
		if strings.Contains(lowerUnaccent(person["name"].(string)), s) || strings.Contains(lowerUnaccent(person["phone"].(string)), s) {
			person["id"] = strconv.Itoa(id)
			data.docs = append(data.docs, person)
		}
		return true
	})
	sort.Sort(data)
	templates.ExecuteTemplate(w, "search.html", data.docs)
}

func importfile(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	err := req.ParseMultipartForm(32 << 20)
	if err != nil {
		panic(err)
	}
	file, _, err := req.FormFile("importfile")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	bs, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}
	reader, err := xlsx.OpenBinary(bs)
	if err != nil {
		panic(err)
	}

	for _, row := range reader.Sheets[0].Rows {
		if len(row.Cells) < 2 {
			continue
		}
		name := row.Cells[0].Value
		phone := row.Cells[1].Value

		_, err = persons.Insert(map[string]interface{}{"name": name, "phone": phone})
		if err != nil {
			log.Panicf("insert error %s", err)
		}
	}

	http.Redirect(w, req, "/", http.StatusFound)
}

//pages
func index(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	err := templates.ExecuteTemplate(w, "index.html", struct {
		Title  string
		ONLoad string
	}{"tel", ""})
	if err != nil {
		log.Panicf("index error %s", err)
	}
}

func main() {
	go browser.OpenURL(fmt.Sprintf("http://127.0.0.1:%d", *port))
	http.ListenAndServe(fmt.Sprintf(":%d", *port), router)
}
