document.onkeyup = function(evt) {
    evt = evt || window.event;
    if (evt.keyCode == 191) {
        search.focus()
    }
}

function sendForm(form, method) {
    var data = {};
    for (var i = 0; i < form.length; ++i) {
        var input = form[i];
        if (input.name) {
            data[input.name] = input.value;
        }
    }
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open(method, form.action, true)
    xmlHttp.send(JSON.stringify(data))
}

function refresh(query) {
    if (document.getElementById('phone_list')) phone_list.innerHTML = '<div class="mdl-spinner mdl-js-spinner is-active"></div>'
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "/search/" + query, true)
    xmlHttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            phone_list.innerHTML = this.responseText;
        }
    }
    xmlHttp.send(null)
}

function hideNew() {
    card.style.maxHeight = "0px";
}

function showNew() {
    formtitle.innerText = "Nová osoba"
    personid.value = ""
    namenew.parentElement.MaterialTextfield.change("");
    phonenew.parentElement.MaterialTextfield.change("");
    deletebutton.style.display = 'none'
    card.style.maxHeight = "500px";
    main.scrollTop = 0
}

function savePerson() {
    sendForm(formnewperson, personid.value == "" ? "POST" : "PUT")
    refresh(search.value)
    hideNew()
}

function deletePerson() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("DELETE", "/person/" + personid.value, true)
    xmlHttp.send(null)
    refresh(search.value)
    hideNew()
}

function openPerson(id, name, phone) {
    formtitle.innerText = "Osoba"
    personid.value = id
    namenew.parentElement.MaterialTextfield.change(name);
    phonenew.parentElement.MaterialTextfield.change(phone);
    document.querySelector('.mdl-textfield').MaterialTextfield.change();
    deletebutton.style.display = 'inline'
    card.style.maxHeight = "500px";
    main.scrollTop = 0
}

function uploadfile() {
    importfileform.submit()
    phone_list.innerHTML = '<div class="mdl-spinner mdl-js-spinner is-active "></div>'
}

refresh('')
