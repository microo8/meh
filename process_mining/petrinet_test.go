package pm

import (
	"fmt"
	"os"
)

func ExampleAlpha() {
	data := [][]string{
		[]string{"a", "c"},
		[]string{"a", "b", "c"},
		[]string{"a", "c", "b"},
		[]string{"a", "b"},
	}
	pn := Alpha(data)
	fmt.Println(pn)
	f, err := os.Create("meh.dot")
	defer f.Close()
	if err != nil {
		panic(err)
	}
	pn.Write(f)
	//Output:
	//digraph pn {
	//	rankdir=LR;
	//	"a" [shape=box];
	//	"b" [shape=box];
	//	"c" [shape=box];
	//	"d" [shape=box];
	//	"e" [shape=box];
	//	"f" [shape=box];
	//	"P(start)" [label="",shape=circle];
	//	"P(end)" [label="",shape=circle];
	//	"P(a,b)" [label="",shape=circle];
	//	"P(a,c)" [label="",shape=circle];
	//	"P(b,d)" [label="",shape=circle];
	//	"P(c,d)" [label="",shape=circle];
	//	"P(e,f)" [label="",shape=circle];
	//	"P(start)" -> "a";
	//	"P(start)" -> "e";
	//	"f" -> "P(end)";
	//	"d" -> "P(end)";
	//	"a" -> "P(a,b)";
	//	"P(a,b)" -> "b";
	//	"a" -> "P(a,c)";
	//	"P(a,c)" -> "c";
	//	"P(b,d)" -> "d";
	//	"b" -> "P(b,d)";
	//	"c" -> "P(c,d)";
	//	"P(c,d)" -> "d";
	//	"e" -> "P(e,f)";
	//	"P(e,f)" -> "f";
	//}
}
