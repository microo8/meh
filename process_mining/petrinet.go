package pm

import (
	"bytes"
	"fmt"
	"io"
)

//PetriNet ...
type PetriNet struct {
	Transitions           map[string]int
	Places                []string
	Inputs                []string
	Outputs               []string
	PlaceConnections      [][]bool
	TransitionConnections [][]bool
}

func (pn *PetriNet) Write(w io.Writer) {
	w.Write([]byte("digraph pn {\n\trankdir=LR;\n"))
	for trans := range pn.Transitions {
		fmt.Fprintf(w, "\t\"%s\" [shape=box];\n", trans)
	}
	for _, place := range pn.Places {
		fmt.Fprintf(w, "\t\"%s\" [label=\"\",shape=circle];\n", place)
	}
	for p := 0; p < len(pn.Places); p++ {
		for trans, t := range pn.Transitions {
			if pn.PlaceConnections[p][t] {
				fmt.Fprintf(w, "\t\"%s\" -> \"%s\";\n", pn.Places[p], trans)
			}
			if pn.TransitionConnections[t][p] {
				fmt.Fprintf(w, "\t\"%s\" -> \"%s\";\n", trans, pn.Places[p])
			}
		}
	}
	w.Write([]byte("}"))
}

func (pn *PetriNet) String() string {
	buf := bytes.NewBuffer(nil)
	pn.Write(buf)
	return buf.String()
}

//Relations is the type in footprint matrix
type Relations uint8

const (
	//NOCAUSAL (#) a never follows b
	NOCAUSAL Relations = iota
	//CAUSALR (->) after a follows b
	CAUSALR
	//CAUSALL (<-) after b follows a
	CAUSALL
	//PARALLEL (||) after a sometimes follows b an after b sometimes follows a
	PARALLEL
)

func combinations(iterable []int, r int) func() []int {
	pool := iterable
	n := len(pool)
	if r > n {
		return nil
	}
	indices := make([]int, r)
	for i := range indices {
		indices[i] = i
	}
	var result []int
	return func() []int {
		if result == nil {
			result = make([]int, r)
			for i, el := range indices {
				result[i] = pool[el]
			}
			return result
		}
		result = make([]int, r)
		i := r - 1
		for ; i >= 0 && indices[i] == i+n-r; i-- {
		}
		if i < 0 {
			return nil
		}
		indices[i]++
		for j := i + 1; j < r; j++ {
			indices[j] = indices[j-1] + 1
		}
		for ; i < len(indices); i++ {
			result[i] = pool[indices[i]]
		}
		return result
	}
}

func allCombinations(iterableRange int) func() []int {
	iterable := make([]int, iterableRange)
	for index := 0; index < iterableRange; index++ {
		iterable[index] = index
	}
	r := iterableRange - 1
	comb := combinations(iterable, r)
	return func() []int {
		c := comb()
		for c == nil {
			if r == 1 {
				return nil
			}
			r--
			comb = combinations(iterable, r)
			c = comb()
		}
		return c
	}
}

func isSubset(a, b []int) bool {
	if len(a) > len(b) {
		return false
	}
	for _, x := range a {
		xInB := false
		for _, y := range b {
			xInB = xInB || x == y
		}
		if !xInB {
			return false
		}
	}
	return true
}

//Alpha - α-algorithm implementation, creates a new PetriNet from eventLog
func Alpha(eventLog [][]string) *PetriNet {
	//all seen transitions
	seen := make(map[string]int)
	index := 0
	for _, event := range eventLog {
		for _, transition := range event {
			if _, ok := seen[transition]; !ok {
				seen[transition] = index
				index++
			}
		}
	}
	fmt.Println(seen)
	seenSlice := make([]string, len(seen))
	for trans, i := range seen {
		seenSlice[i] = trans
	}
	//starting and ending transitions
	Ti := make(map[string]bool)
	To := make(map[string]bool)
	for _, event := range eventLog {
		Ti[event[0]] = true
		To[event[len(event)-1]] = true
	}
	directTransition := make([][]bool, len(seen))
	for index = 0; index < len(seen); index++ {
		directTransition[index] = make([]bool, len(seen))
	}
	for _, event := range eventLog {
		for index = 0; index < len(event)-1; index++ {
			directTransition[seen[event[index]]][seen[event[index+1]]] = true
		}
	}
	footprint := make([][]Relations, len(seen))
	for index = 0; index < len(seen); index++ {
		footprint[index] = make([]Relations, len(seen))
	}
	for a := 0; a < len(seen); a++ {
		for b := 0; b < len(seen); b++ {
			if directTransition[a][b] {
				if directTransition[b][a] {
					footprint[a][b] = PARALLEL
				} else {
					footprint[a][b] = CAUSALR
					footprint[b][a] = CAUSALL
				}
			} else if directTransition[b][a] {
				footprint[a][b] = CAUSALL
				footprint[b][a] = CAUSALR
			}
		}
	}
	fmt.Println("footprint", footprint)

	var xl [][][]int
	//generate all subsets of seen
	comb1 := allCombinations(len(seen))
	for A := comb1(); A != nil; A = comb1() {
		//check if all elements in A are not causal
		nocausalA := true
		for activity1Index := 0; nocausalA && activity1Index < len(A); activity1Index++ {
			for activity2Index := activity1Index; nocausalA && activity2Index < len(A); activity2Index++ {
				nocausalA = nocausalA && footprint[A[activity1Index]][A[activity2Index]] == NOCAUSAL
			}
		}
		if !nocausalA {
			continue
		}
		comb2 := allCombinations(len(seen))
		for B := comb2(); B != nil; B = comb2() {
			//check if all elements in subSet2 are not causal
			nocausalB := true
			for activity1Index := 0; nocausalB && activity1Index < len(B); activity1Index++ {
				for activity2Index := activity1Index; nocausalB && activity2Index < len(B); activity2Index++ {
					nocausalB = nocausalB && footprint[B[activity1Index]][B[activity2Index]] == NOCAUSAL
				}
			}
			if !nocausalB {
				continue
			}
			causal := true
			for activity1Index := 0; causal && activity1Index < len(A); activity1Index++ {
				for activity2Index := 0; causal && activity2Index < len(B); activity2Index++ {
					causal = causal && footprint[A[activity1Index]][B[activity2Index]] == CAUSALR
				}
			}
			if !causal {
				continue
			}
			xl = append(xl, [][]int{A, B})
			fmt.Print("xl {")
			for i, p := range A {
				fmt.Print(seenSlice[p])
				if i+1 != len(A) {
					fmt.Print(",")
				}
			}
			fmt.Print("} {")
			for i, p := range B {
				fmt.Print(seenSlice[p])
				if i+1 != len(B) {
					fmt.Print(",")
				}
			}
			fmt.Println("}")
		}
	}
	//add all pairs that dont have subsets in the rest
	var yl [][][]int
	for i, pair1 := range xl {
		subset := false
		for j, pair2 := range xl {
			if i == j {
				continue
			}
			subset = subset || (isSubset(pair1[0], pair2[0]) && isSubset(pair1[1], pair2[1]))
			if subset {
				fmt.Println(pair1[0], "is subset of", pair2[0], "and", pair1[1], "is subset of", pair2[1])
				break
			}
		}
		if subset {
			continue
		}
		yl = append(yl, pair1)
		fmt.Print("yl {")
		for i, p := range pair1[0] {
			fmt.Print(seenSlice[p])
			if i+1 != len(pair1[0]) {
				fmt.Print(",")
			}
		}
		fmt.Print("} {")
		for i, p := range pair1[1] {
			fmt.Print(seenSlice[p])
			if i+1 != len(pair1[1]) {
				fmt.Print(",")
			}
		}
		fmt.Println("}")
	}
	//create a PetriNet
	pn := new(PetriNet)
	pn.Transitions = seen
	pn.Places = make([]string, len(yl)+2)
	pn.Places[0] = "P(start)"
	pn.Places[1] = "P(end)"
	for i, pair := range yl {
		buf := bytes.NewBuffer([]byte("P("))
		for _, t := range pair[0] {
			buf.WriteString(seenSlice[t])
		}
		buf.WriteRune(',')
		for _, t := range pair[1] {
			buf.WriteString(seenSlice[t])
		}
		buf.WriteRune(')')
		pn.Places[i+2] = buf.String()
	}
	pn.Inputs = make([]string, len(Ti))
	index = 0
	for trans := range Ti {
		pn.Inputs[index] = trans
		index++
	}
	pn.Outputs = make([]string, len(To))
	index = 0
	for trans := range To {
		pn.Outputs[index] = trans
		index++
	}
	pn.PlaceConnections = make([][]bool, len(pn.Places))
	for i := 0; i < len(pn.Places); i++ {
		pn.PlaceConnections[i] = make([]bool, len(pn.Transitions))
	}
	for _, i := range pn.Inputs {
		pn.PlaceConnections[0][seen[i]] = true //from start place to all inputs
	}
	pn.TransitionConnections = make([][]bool, len(pn.Transitions))
	for i := 0; i < len(pn.Transitions); i++ {
		pn.TransitionConnections[i] = make([]bool, len(pn.Places))
	}
	for _, o := range pn.Outputs {
		pn.TransitionConnections[seen[o]][1] = true //all outputs connected to end place
	}
	for i, pair := range yl {
		for _, p := range pair[0] {
			pn.TransitionConnections[p][i+2] = true
		}
		for _, p := range pair[1] {
			pn.PlaceConnections[i+2][p] = true
		}
	}
	return pn
}
