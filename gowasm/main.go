package main

import (
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	log.Println("Hello, WebAssembly!")
	resp, err := http.Get("index.html")
	if err != nil {
		log.Panic(err)
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Panic(err)
	}
	log.Println(string(data))
}
