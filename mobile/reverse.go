// Copyright 2016 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package reverse implements an Android app in 100% Go.

// +build android

package reverse


import (
	"Java/android/support/v7/app"
	"Java/android/os"
	"Java/java/lang/Float"
	gopkg "Java/reverse"
)

type MainActivity struct {
	app.AppCompatActivity

	//webView webkit.WebView
}

func (a *MainActivity) OnCreate(this gopkg.MainActivity, b os.Bundle) {
	this.Super().OnCreate(b)
	/*
		        mWebView = new WebView(this);
				WebSettings webSettings = mWebView.getSettings();
				webSettings.setJavaScriptEnabled(true);
				mWebView.setWebViewClient(new WebViewClient());
		        setContentView(mWebView);
	*/
}

func (a *MainActivity) OnDestroy(this gopkg.MainActivity) {
	this.Super().OnDestroy()
}

func (a *MainActivity) GetLabel() string {
	return "Hello from Go!"
}
