// +build android

package test1

import (
	gopkg "Java/test1"
    "Java/android/app"
	"Java/android/os"
	"Java/android/webkit"
)


type MainActivity struct {
	app.Activity
	MyWebView webkit.WebView
}

func (a *MainActivity) OnCreate(this gopkg.MainActivity, b os.Bundle) {
	this.Super().OnCreate(b)
	a.MyWebView = webkit.WebView.New(a)
}

func (a *MainActivity) OnDestroy(this gopkg.MainActivity) {
	this.Super().OnDestroy()
}
