package main

import (
	"flag"
	"fmt"

	"github.com/syndtr/goleveldb/leveldb"
)

func main() {
	flag.Parse()
	db, err := leveldb.OpenFile(flag.Arg(0), nil)
	if err != nil {
		fmt.Println(err)
	}
	it := db.NewIterator(nil, nil)
	for it.Next() {
		var value string
		if it.Value() != nil {
			value = string(it.Value())
		}
		fmt.Println(string(it.Key()), value)
	}
}
