package uiml

import (
	"bufio"
	"fmt"
)

type itemType int

const (
	itemError itemType = iota

	itemDot
	itemEOF
	itemLeftBrace
	itemRightBrace
	itemColon
	itemSemicolon
	itemNumber
	itemString
	itemIdent

	itemImport

	eof = rune(0)
)

//item represents a token returned from the scanner
type item struct {
	typ itemType
	val string
}

func (i item) String() string {
	switch i.typ {
	case itemEOF:
		return "EOF"
	case itemError:
		return i.val
	}
	if len(i.val) > 10 {
		return fmt.Sprintf("%.10q...", i.val)
	}
	return fmt.Sprintf("%q", i.val)
}

type stateFn func(*lexer) stateFn

type lexer struct {
	r                  bufio.Reader
	line, pos, prevpos int
}

// read reads the next rune from the bufferred reader.
// Returns the rune(0) if an error occurs (or io.EOF is returned).
func (l *lexer) read() rune {
	ch, _, err := l.r.ReadRune()
	if ch == '\n' {
		l.line++
		l.prevpos = l.pos
		l.pos = 0
	} else {
		l.pos++
		if err != nil {
			return eof
		}
	}
	return ch
}

// unread places the previously read rune back on the reader.
func (l *lexer) unread() {
	l.r.UnreadRune()
	if l.pos == 0 {
		l.pos = l.prevpos
		if l.line == 0 {
			panic("Cannot unread! No runes readed")
		}
		l.line--
	} else {
		l.pos--
	}
}
