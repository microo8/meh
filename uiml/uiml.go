package uiml

import (
	"fmt"
	"io"
	"os"
	"runtime"

	"github.com/go-gl/glfw/v3.2/glfw"
)

var (
	window *glfw.Window
	widget Widget
)

//Init initializes the uiml runtime and window
func Init(width, height int, title string) error {
	runtime.LockOSThread()
	err := glfw.Init()
	if err != nil {
		return err
	}
	window, err = glfw.CreateWindow(width, height, title, nil, nil)
	return err
}

//Run runs the main loop
func Run() error {
	if window == nil {
		return fmt.Errorf("uiml not initialized, window not created")
	}
	defer glfw.Terminate()
	window.MakeContextCurrent()

	for !window.ShouldClose() {
		if err := widget.Draw(); err != nil {
			return err
		}
		window.SwapBuffers()
		glfw.PollEvents()
	}
	return nil
}

//AddFromFile adds an uiml source to render
func AddFromFile(path string) error {
	f, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("Error adding file to uiml runtime: %s", err)
	}
	return Add(f)
}

//Add adds an uiml source to render
func Add(r io.Reader) error {
	s := NewScanner(r)
	`
	return nil
}
