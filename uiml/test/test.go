package main

import "github.com/microo8/meh/uiml"

func init() {
	// This is needed to arrange that main() runs on main thread.
	// See documentation for functions that are only allowed to be called from the main thread.
	err := uiml.Init(640, 480, "uiml test")
	if err != nil {
		panic(err)
	}
}

func main() {
	if err := uiml.AddFromFile("test.uiml"); err != nil {
		panic(err)
	}
	if err := uiml.Run(); err != nil {
		panic(err)
	}
}
