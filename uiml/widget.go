package uiml

//Widget interface specifies the default uiml type
type Widget interface {
	ID() string
	Draw() error
}
