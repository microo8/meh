package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/skratchdot/open-golang/open"
)

var (
	outputxlsx = flag.String("o", "report.xlsx", "output file")
	month      = flag.String("m", "", "month in format: YYYY-MM (defaults to previous month)")
	apiToken   = flag.String("t", "", "toggl api token (https://toggl.com/app/profile)")
	workspace  = flag.String("w", "workspace", "workspace name")

	client = &http.Client{Timeout: 5 * time.Second}
)

type dur struct {
	time.Duration
}

func (d *dur) UnmarshalJSON(b []byte) error {
	duration, err := strconv.Atoi(string(b))
	if err != nil {
		return err
	}
	d.Duration = time.Millisecond * time.Duration(duration)
	return nil
}

type entry struct {
	User        string    `json:"user"`
	Client      string    `json:"client"`
	Project     string    `json:"project"`
	Description string    `json:"description"`
	Start       time.Time `json:"start"`
	End         time.Time `json:"end"`
	Duration    dur       `json:"dur"`
	Tags        []string  `json:"tags"`
}

func getFirstEntry(data map[int][]entry) *entry {
	for _, es := range data {
		if len(es) == 0 {
			continue
		}
		return &es[0]
	}
	return nil
}

func getEntries(workspaceID int) ([]entry, error) {
	var t time.Time
	var err error
	if *month == "" {
		t = time.Now().AddDate(0, -1, 0)
	} else {
		t, err = time.Parse("2006-01", *month)
		if err != nil {
			return nil, fmt.Errorf("error parsing provided month: %s (must be YYYY-MM)", err)
		}
	}
	firstOfMonth := time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, t.Location())
	lastOfMonth := firstOfMonth.AddDate(0, 1, -1)
	since := firstOfMonth.Format("2006-01-02")
	until := lastOfMonth.Format("2006-01-02")
	url := fmt.Sprintf(
		"https://toggl.com/reports/api/v2/details?user_agent=togglxlsx&workspace_id=%d&since=%s&until=%s",
		workspaceID,
		since,
		until,
	)
	var res []entry
	page := 1
	for {
		req, err := http.NewRequest("GET", fmt.Sprintf("%s&page=%d", url, page), nil)
		if err != nil {
			return nil, fmt.Errorf("creating report request: %s", err)
		}
		req.SetBasicAuth(*apiToken, "api_token")
		resp, err := client.Do(req)
		if err != nil {
			return nil, fmt.Errorf("error getting report: %s", err)
		}
		if resp.StatusCode != http.StatusOK {
			return nil, fmt.Errorf("error getting report: status code is %d", resp.StatusCode)
		}
		var data struct{ Data []entry }
		if err = json.NewDecoder(resp.Body).Decode(&data); err != nil {
			return nil, fmt.Errorf("error decoding report: %s", err)
		}
		if len(data.Data) == 0 {
			break
		}
		res = append(res, data.Data...)
		page++
	}
	return res, nil
}

func getWorkspaceID() (int, error) {
	req, err := http.NewRequest("GET", "https://www.toggl.com/api/v8/workspaces?user_agent=togglxlsx", nil)
	if err != nil {
		return 0, fmt.Errorf("error creating request: %s", err)
	}
	req.SetBasicAuth(*apiToken, "api_token")
	resp, err := client.Do(req)
	if err != nil {
		return 0, fmt.Errorf("error getting workspaces: %s", err)
	}
	if resp.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("error getting workspaces: status code is %d", resp.StatusCode)
	}
	var workspaces []struct {
		ID   int
		Name string
	}
	if err = json.NewDecoder(resp.Body).Decode(&workspaces); err != nil {
		return 0, fmt.Errorf("error decoding response: %s", err)
	}
	if len(workspaces) == 0 {
		return 0, fmt.Errorf("error: user has no toggl workspaces")
	}
	var workspaceID int
	for _, w := range workspaces {
		if w.Name != *workspace {
			continue
		}
		workspaceID = w.ID
	}
	if workspaceID == 0 {
		return 0, fmt.Errorf("error: workspace with name '%s' not found", *workspace)
	}
	return workspaceID, nil
}

func main() {
	flag.Parse()
	if *apiToken == "" {
		fmt.Fprintf(os.Stderr, "parameter api token if required")
		os.Exit(1)
	}
	workspaceID, err := getWorkspaceID()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s", err)
		os.Exit(1)
	}
	entries, err := getEntries(workspaceID)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error getting entries: %s", err)
		os.Exit(1)
	}
	if len(entries) == 0 {
		fmt.Fprintf(os.Stderr, "no entries")
		os.Exit(1)
	}
	data := make(map[int][]entry)
	for _, e := range entries {
		data[e.Start.Day()-1] = append(data[e.Start.Day()-1], e)
	}

	xlsx := excelize.NewFile()
	//styles
	centerStyle, err := xlsx.NewStyle(`{"alignment":{"horizontal":"center"}}`)
	if err != nil {
		panic(err)
	}
	centerBoldStyle, err := xlsx.NewStyle(`{"alignment":{"horizontal":"center"},"font":{"bold":true}}`)
	if err != nil {
		panic(err)
	}
	dateStyle, err := xlsx.NewStyle(`{"custom_number_format": "dd.mm.", "alignment":{"horizontal":"center"}}`)
	if err != nil {
		panic(err)
	}
	timeStyle, err := xlsx.NewStyle(`{"custom_number_format": "hh:mm:ss", "alignment":{"horizontal":"center"}}`)
	if err != nil {
		panic(err)
	}
	durationStyle, err := xlsx.NewStyle(`{"custom_number_format": "[hh]:mm:ss", "alignment":{"horizontal":"center"}}`)
	if err != nil {
		panic(err)
	}

	//detail sheet
	detailSheet := "Detail"
	xlsx.SetSheetName("Sheet1", detailSheet)
	xlsx.SetCellValue(detailSheet, "A1", "Client")
	xlsx.SetCellValue(detailSheet, "B1", "Project")
	xlsx.SetCellValue(detailSheet, "C1", "Description")
	xlsx.SetCellValue(detailSheet, "D1", "Start")
	xlsx.SetCellValue(detailSheet, "E1", "End")
	xlsx.SetCellValue(detailSheet, "F1", "Duration")
	xlsx.SetCellValue(detailSheet, "G1", "Tags")

	for i, e := range entries {
		xlsx.SetCellValue(detailSheet, fmt.Sprintf("A%d", i+2), e.Client)
		xlsx.SetCellValue(detailSheet, fmt.Sprintf("B%d", i+2), e.Project)
		xlsx.SetCellValue(detailSheet, fmt.Sprintf("C%d", i+2), e.Description)
		xlsx.SetCellValue(detailSheet, fmt.Sprintf("D%d", i+2), e.Start)
		xlsx.SetCellValue(detailSheet, fmt.Sprintf("E%d", i+2), e.End)
		xlsx.SetCellValue(detailSheet, fmt.Sprintf("F%d", i+2), e.Duration)
		xlsx.SetCellValue(detailSheet, fmt.Sprintf("G%d", i+2), strings.Join(e.Tags, ","))
	}

	// sumary sheet
	e := entries[0]
	sheetName := e.Start.Format("January 2006")
	index := xlsx.NewSheet(sheetName)
	xlsx.SetCellValue(sheetName, "A1", "Dochádzková karta")
	xlsx.SetCellValue(sheetName, "B1", "Mesačný prehľad")
	xlsx.SetCellValue(sheetName, "A3", "Za mesiac:")
	xlsx.SetCellValue(sheetName, "B3", sheetName)
	xlsx.SetCellValue(sheetName, "A4", "Meno:")
	xlsx.SetCellValue(sheetName, "B4", e.User)

	xlsx.SetCellStyle(sheetName, "A6", "G6", centerBoldStyle)
	xlsx.SetCellValue(sheetName, "A6", "Dátum")
	xlsx.SetCellValue(sheetName, "B6", "Klient")
	xlsx.SetCellValue(sheetName, "C6", "Projekt")
	xlsx.SetCellValue(sheetName, "D6", "Začiatok")
	xlsx.SetCellValue(sheetName, "E6", "Koniec")
	xlsx.SetCellValue(sheetName, "F6", "Celkom")
	xlsx.SetCellValue(sheetName, "G6", "Popis")
	xlsx.SetColWidth(sheetName, "A", "F", 19)
	xlsx.SetColWidth(sheetName, "G", "G", 40)

	d1 := time.Date(e.Start.Year(), e.Start.Month(), 1, 0, 0, 0, 0, time.UTC)
	row := 7
	for d := d1; d.Month() == d1.Month(); d = d.AddDate(0, 0, 1) {
		groups := groupEntries(data[d.Day()-1])
		for _, e := range groups {
			xlsx.SetCellStyle(sheetName, fmt.Sprintf("A%d", row), fmt.Sprintf("A%d", row), dateStyle)
			xlsx.SetCellStyle(sheetName, fmt.Sprintf("B%d", row), fmt.Sprintf("C%d", row), centerStyle)
			xlsx.SetCellStyle(sheetName, fmt.Sprintf("D%d", row), fmt.Sprintf("E%d", row), timeStyle)
			xlsx.SetCellStyle(sheetName, fmt.Sprintf("F%d", row), fmt.Sprintf("F%d", row), durationStyle)
			xlsx.SetCellStyle(sheetName, fmt.Sprintf("G%d", row), fmt.Sprintf("G%d", row), centerStyle)
			xlsx.SetCellValue(sheetName, fmt.Sprintf("A%d", row), d)
			xlsx.SetCellValue(sheetName, fmt.Sprintf("B%d", row), e.Client)
			xlsx.SetCellValue(sheetName, fmt.Sprintf("C%d", row), e.Project)
			xlsx.SetCellValue(sheetName, fmt.Sprintf("D%d", row), e.Start)
			xlsx.SetCellValue(sheetName, fmt.Sprintf("E%d", row), e.End)
			xlsx.SetCellValue(sheetName, fmt.Sprintf("F%d", row), e.Duration.Seconds()/86400)
			xlsx.SetCellValue(sheetName, fmt.Sprintf("G%d", row), e.Description)
			row++
		}
		if len(groups) == 0 {
			xlsx.SetCellStyle(sheetName, fmt.Sprintf("A%d", row), fmt.Sprintf("A%d", row), dateStyle)
			xlsx.SetCellValue(sheetName, fmt.Sprintf("A%d", row), d)
			row++
		}
	}
	xlsx.SetCellValue(sheetName, fmt.Sprintf("E%d", row), "Spolu:")
	xlsx.SetCellStyle(sheetName, fmt.Sprintf("F%d", row), fmt.Sprintf("F%d", row), durationStyle)
	xlsx.SetCellFormula(sheetName, fmt.Sprintf("F%d", row), fmt.Sprintf("=SUM(F7:F%d)", row-1))

	//projects pivot table
	row += 4
	xlsx.SetCellStyle(sheetName, fmt.Sprintf("A%d", row), fmt.Sprintf("G%d", row), centerBoldStyle)
	xlsx.SetCellValue(sheetName, fmt.Sprintf("A%d", row), "Projekt")
	xlsx.SetCellValue(sheetName, fmt.Sprintf("B%d", row), "Celkom")
	projectsPivot := make(PivotTable)
	for _, e := range entries {
		projectsPivot.Add(e.Project, e.Duration.Duration)
	}
	row++
	pivotStart := row
	xlsx.SetCellStyle(sheetName, fmt.Sprintf("B%d", row), fmt.Sprintf("B%d", row+len(projectsPivot)), durationStyle)
	for _, p := range projectsPivot.Sorted() {
		if p.Key == "" {
			xlsx.SetCellValue(sheetName, fmt.Sprintf("A%d", row), "-")
		} else {
			xlsx.SetCellValue(sheetName, fmt.Sprintf("A%d", row), p.Key)
		}
		xlsx.SetCellValue(sheetName, fmt.Sprintf("B%d", row), p.Value.Seconds()/86400)
		row++
	}
	pivotEnd := row

	xlsx.AddChart(
		sheetName,
		fmt.Sprintf("A%d", pivotEnd+1),
		fmt.Sprintf(
			`{"type":"pie","title":{"name":"Podľa projektov"},"series":[{"categories":"='%s'!$A$%d:$A$%d", "values":"='%s'!$B$%d:$B$%d"}],"format":{"x_scale":0.6,"y_scale":1.0}}`,
			sheetName, pivotStart, pivotEnd-1,
			sheetName, pivotStart, pivotEnd-1,
		),
	)

	//clients pivot table
	row = pivotStart - 1
	xlsx.SetCellStyle(sheetName, fmt.Sprintf("A%d", row), fmt.Sprintf("G%d", row), centerBoldStyle)
	xlsx.SetCellValue(sheetName, fmt.Sprintf("E%d", row), "Klient")
	xlsx.SetCellValue(sheetName, fmt.Sprintf("F%d", row), "Celkom")
	clientsPivot := make(PivotTable)
	for _, e := range entries {
		clientsPivot.Add(e.Client, e.Duration.Duration)
	}
	row++
	xlsx.SetCellStyle(sheetName, fmt.Sprintf("F%d", row), fmt.Sprintf("F%d", row+len(clientsPivot)), durationStyle)
	for _, p := range clientsPivot.Sorted() {
		if p.Key == "" {
			xlsx.SetCellValue(sheetName, fmt.Sprintf("E%d", row), "-")
		} else {
			xlsx.SetCellValue(sheetName, fmt.Sprintf("E%d", row), p.Key)
		}
		xlsx.SetCellValue(sheetName, fmt.Sprintf("F%d", row), p.Value.Seconds()/86400)
		row++
	}

	xlsx.AddChart(
		sheetName,
		fmt.Sprintf("E%d", pivotEnd+1),
		fmt.Sprintf(
			`{"type":"pie","title":{"name":"Podľa klientov"},"series":[{"categories":"='%s'!$E$%d:$E$%d", "values":"='%s'!$F$%d:$F$%d"}],"format":{"x_scale":0.6,"y_scale":1.0}}`,
			sheetName, pivotStart, row-1,
			sheetName, pivotStart, row-1,
		),
	)

	xlsx.SetActiveSheet(index)
	// Save xlsx file by the given path.
	if err := xlsx.SaveAs(*outputxlsx); err != nil {
		fmt.Fprintf(os.Stderr, "Error writing file %s: %s", *outputxlsx, err)
		return
	}
	open.Run(*outputxlsx)
}

func groupEntries(es []entry) (ees []entry) {
	groups := make(map[string][]entry)
	for _, e := range es {
		key := e.User + e.Client + e.Project + e.Description + strings.Join(e.Tags, ",")
		groups[key] = append(groups[key], e)
	}
	for _, group := range groups {
		start := group[0].Start
		end := group[0].End
		var duration time.Duration
		for _, g := range group {
			if start.After(g.Start) {
				start = g.Start
			}
			if end.Before(g.End) {
				end = g.End
			}
			duration += g.Duration.Duration
		}
		ees = append(
			ees,
			entry{
				User:        group[0].User,
				Client:      group[0].Client,
				Project:     group[0].Project,
				Description: group[0].Description,
				Tags:        group[0].Tags,
				Start:       start,
				End:         end,
				Duration:    dur{duration},
			},
		)
	}
	return
}

//PivotTable used to sum up the duration by category and get the sorted data
type PivotTable map[string]time.Duration

//Add adds new duration to the PivotTable
func (pt PivotTable) Add(category string, duration time.Duration) {
	pt[category] += duration
}

//Sorted returns an sorted list of key-value pairs
func (pt PivotTable) Sorted() PairList {
	pl := make(PairList, len(pt))
	i := 0
	for k, v := range pt {
		pl[i] = Pair{k, v}
		i++
	}
	sort.Sort(sort.Reverse(pl))
	return pl
}

//Pair represents a kev-value pair
type Pair struct {
	Key   string
	Value time.Duration
}

//PairList is a sortable list of key-value pairs
type PairList []Pair

func (p PairList) Len() int           { return len(p) }
func (p PairList) Less(i, j int) bool { return p[i].Value < p[j].Value }
func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
