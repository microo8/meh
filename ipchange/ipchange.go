package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/smtp"
	"os"
	"path"
	"strings"

	"github.com/shibukawa/configdir"
)

var (
	wtfipurl = "https://wtfismyip.com/text"
	fpath    = configdir.New("microo8", "ipchange").QueryCacheFolder().Path
	from     = os.Getenv("IPCHANGE_FROM")
	to       = strings.Split(os.Getenv("IPCHANGE_TO"), ";")
	pass     = os.Getenv("IPCHANGE_PASS")
)

func getIP() (string, error) {
	resp, err := http.Get(wtfipurl)
	if err != nil {
		return "", err
	}
	ip, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(ip), nil
}

func writeIP(ip string) error {
	if _, err := os.Stat(fpath); os.IsNotExist(err) {
		err = os.MkdirAll(fpath, 0777)
		if err != nil {
			return err
		}
	}
	f, err := os.Create(path.Join(fpath, "ip"))
	if err != nil {
		return err
	}
	_, err = f.Write([]byte(ip))
	return err
}

func readIP() (string, error) {
	f, err := os.Open(path.Join(fpath, "ip"))
	if err != nil {
		return "", err
	}
	ip, err := ioutil.ReadAll(f)
	if err != nil {
		return "", err
	}
	return string(ip), nil
}

func main() {
	newIP, err := getIP()
	if err != nil {
		fmt.Println(err)
		return
	}
	oldIP, _ := readIP()
	if newIP == oldIP {
		return
	}
	err = writeIP(newIP)
	if err != nil {
		fmt.Println("write error:", err)
		return
	}
	msg := fmt.Sprintf("From: %s\nTo: %s\nSubject: IP change\n\nThe new ip address: %s", from, to, newIP)
	err = smtp.SendMail("smtp.gmail.com:587", smtp.PlainAuth("", from, pass, "smtp.gmail.com"), from, to, []byte(msg))
	if err != nil {
		fmt.Println(err)
		return
	}
}
