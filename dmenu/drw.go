package main

/*
#cgo LDFLAGS: -lX11
#include <X11/Xlib.h>
*/
import "C"

import (
	"fmt"
	"image/color"
)

func parseColor(hex string) color.Color {
	var c color.RGBA
	fmt.Sscanf(hex, "#%02x%02x%02x", &c.R, &c.G, &c.B)
	return c
}

//Drw ...
type Drw struct {
	w, h     uint
	dpy      *C.Display
	screen   int
	root     C.Window
	drawable C.Drawable
	gc       C.GC
	scheme   [][]color.Color
}

//NewDrw ...
func NewDrw(dpy *C.Display, screen int, root C.Window, w, h uint) *Drw {
	drw := &Drw{
		dpy:      dpy,
		screen:   screen,
		root:     root,
		w:        w,
		h:        h,
		drawable: (C.Drawable)(C.XCreatePixmap(dpy, (C.Drawable)(root), C.uint(w), C.uint(h), C.uint(C.XDefaultDepth(dpy, C.int(screen))))),
		gc:       C.XCreateGC(dpy, (C.Drawable)(root), 0, nil),
	}
	C.XSetLineAttributes(dpy, drw.gc, 1, C.LineSolid, C.CapButt, C.JoinMiter)

	return drw
}

func (drw *Drw) scmCreate(clrnames []string) []color.Color {
	/* need at least two colors for a scheme */
	if drw == nil || len(clrnames) < 2 {
		return nil
	}

	ret := make([]color.Color, len(clrnames))
	for i, clr := range clrnames {
		ret[i] = parseColor(clr)
	}
	return ret
}

func (drw *Drw) resize(w, h uint) {
	if drw == nil {
		return
	}

	drw.w = w
	drw.h = h
	//if drw.drawable != nil {
	C.XFreePixmap(drw.dpy, (C.Pixmap)(drw.drawable))
	drw.drawable = (C.Drawable)(C.XCreatePixmap(drw.dpy, (C.Drawable)(drw.root), C.uint(drw.w), C.uint(drw.h), C.uint(C.XDefaultDepth(drw.dpy, C.int(drw.screen)))))
}

func (drw *Drw) rect(x, y int, w, h uint, filled, invert bool) {
	fmt.Println("rect", x, y, w, h)
	if drw == nil || drw.scheme == nil {
		return
	}
	if invert {
		C.XSetForeground(drw.dpy, drw.gc, 0) //TODO drw.scheme[ColBg].pixel)
	} else {
		C.XSetForeground(drw.dpy, drw.gc, 0) //TODO drw.scheme[ColFg].pixel)
	}
	if filled {
		C.XFillRectangle(drw.dpy, drw.drawable, drw.gc, C.int(x), C.int(y), C.uint(w), C.uint(h))
		return
	}
	C.XDrawRectangle(drw.dpy, drw.drawable, drw.gc, C.int(x), C.int(y), C.uint(w-1), C.uint(h-1))
}

//Map ...
func (drw *Drw) Map(win C.Window, x, y int, w, h uint) {
	if drw == nil {
		return
	}
	C.XCopyArea(drw.dpy, drw.drawable, (C.Drawable)(win), drw.gc, C.int(x), C.int(y), C.uint(w), C.uint(h), C.int(x), C.int(y))
	C.XSync(drw.dpy, C.False)
}
