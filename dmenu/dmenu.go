package main

/*
#cgo LDFLAGS: -lX11
#include <locale.h>
#include <X11/Xlib.h>

XIC CreateIC(XIM xim, int arg1, int arg2, int arg3, int arg4, int arg5, int arg6, int arg7) {
	return XCreateIC(xim, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
}
int xevent_type(XEvent *xevent) {
	return xevent->type;
}
XExposeEvent xevent_xexpose(XEvent *xevent) {
	return xevent->xexpose;
}
XFocusChangeEvent xevent_xfocus(XEvent *xevent) {
	return xevent->xfocus;
}
XKeyEvent xevent_xkey(XEvent *xevent) {
	return xevent->xkey;
}
XSelectionEvent xevent_xselection(XEvent *xevent) {
	return xevent->xselection;
}
XVisibilityEvent xevent_xvisibility(XEvent *xevent) {
	return xevent->xvisibility;
}
*/
import "C"

import (
	"fmt"
	"os"
	"time"
)

var (
	dpy        *C.Display
	screen     C.int
	root, win  C.Window
	drw        *Drw
	lines      = 3
	mw, mh     C.uint
	clip, utf8 C.Atom
)

func init() {
	if C.setlocale(C.LC_CTYPE, C.CString("")) == nil || C.XSupportsLocale() == 0 {
		fmt.Fprintf(os.Stderr, "warning: no locale support\n")
	}
	if dpy = C.XOpenDisplay(C.CString("")); dpy == nil {
		panic("cannot open display")
	}
	//if (!embed || !(parentwin = strtol(embed, NULL, 0))) parentwin = root;
	screen = C.XDefaultScreen(dpy)
	root = C.XRootWindow(dpy, screen)
	var wa C.XWindowAttributes
	if C.XGetWindowAttributes(dpy, root, &wa) == 0 {
		fmt.Fprintf(os.Stderr, "could not get embedding window attributes\n")
	}
	drw = NewDrw(dpy, int(screen), root, uint(wa.width), uint(wa.height))
	clip = C.XInternAtom(dpy, C.CString("CLIPBOARD"), C.False)
	utf8 = C.XInternAtom(dpy, C.CString("UTF8_STRING"), C.False)
}

func main() {
	/*
		if (!drw_fontset_create(drw, fonts, LENGTH(fonts)))
			die("no fonts could be loaded.");
		lrpad = drw.fonts.h;
	*/

	grabkeyboard()
	setup()
	run()
}

func grabkeyboard() {
	/* try to grab keyboard, we may have to wait for another process to ungrab */
	for i := 0; i < 1000; i++ {
		if C.XGrabKeyboard(
			dpy,
			C.XDefaultRootWindow(dpy),
			C.True, C.GrabModeAsync, C.GrabModeAsync, C.CurrentTime) == C.GrabSuccess {
			return
		}
		time.Sleep(time.Millisecond)
	}
	panic("cannot grab keyboard")
}

func setup() {
	/*
		int x, y, i = 0;
		unsigned int du;
		XIM xim;
		Window w, dw, *dws;
	*/

	/*
		// init appearance
		scheme[SchemeNorm] = drw.scmCreate(colors[SchemeNorm])
		scheme[SchemeSel] = drw.scmCreate(colors[SchemeSel])
		scheme[SchemeOut] = drw.scmCreate(colors[SchemeOut])
	*/

	// calculate menu geometry
	//bh := int(basicfont.Face7x13.Metrics().Height + 2)
	mh = 50 //C.uint((lines + 1) * bh)
	var wa C.XWindowAttributes
	if C.XGetWindowAttributes(dpy, root, &wa) == 0 {
		panic("could not get embedding window attributes")
	}
	mw = C.uint(wa.width)
	//TODO match();

	// create menu window
	var swa C.XSetWindowAttributes
	swa.override_redirect = C.True
	swa.background_pixel = 0 //TODO scheme[SchemeNorm][ColBg].pixel
	swa.event_mask = C.ExposureMask | C.KeyPressMask | C.VisibilityChangeMask
	win = C.XCreateWindow(dpy, root,
		0, 0, C.uint(mw), C.uint(mh), 0,
		C.CopyFromParent, C.CopyFromParent, nil,
		C.CWOverrideRedirect|C.CWBackPixel|C.CWEventMask, &swa)

	// open input methods
	xim := C.XOpenIM(dpy, nil, nil, nil)
	xic := C.CreateIC(xim, C.XNInputStyle, C.XIMPreeditNothing|C.XIMStatusNothing, C.XNClientWindow, win, C.XNFocusWindow, win, nil)
	C.XMapRaised(dpy, win)
	drw.resize(uint(mw), uint(mh))
	drawmenu()
}

func drawmenu() {
	/*
		unsigned int curpos;
		struct item *item;
		int x = 0, y = 0, w;
	*/

	//drw_setscheme(drw, scheme[SchemeNorm]);
	drw.rect(0, 0, uint(mw), uint(mh), true, true)

	/*
		if (lines > 0) {
			// draw vertical list
			for (item = curr; item != next; item = item.right)
				drawitem(item, x, y += bh, mw - x);
		} else if (matches) {
			// draw horizontal list
			x += inputw;
			w = TEXTW("<");
			if (curr.left) {
				drw_setscheme(drw, scheme[SchemeNorm]);
				drw_text(drw, x, 0, w, bh, lrpad / 2, "<", 0);
			}
			x += w;
			for (item = curr; item != next; item = item.right)
				x = drawitem(item, x, 0, MIN(TEXTW(item.text), mw - x - TEXTW(">")));
			if (next) {
				w = TEXTW(">");
				drw_setscheme(drw, scheme[SchemeNorm]);
				drw_text(drw, mw - w, 0, w, bh, lrpad / 2, ">", 0);
			}
		}
		drw_map(drw, win, 0, 0, mw, mh);
	*/
}

func run() {
	var ev C.XEvent
	for {
		C.XNextEvent(dpy, &ev)
		if C.XFilterEvent(&ev, win) != 0 {
			continue
		}
		switch C.xevent_type(&ev) {
		case C.Expose:
			fmt.Println("Expose")
			if C.xevent_xexpose(&ev).count == 0 {
				drw.Map(win, 0, 0, uint(mw), uint(mh))
			}
		case C.FocusIn:
			fmt.Println("FocusIn")
			/* regrab focus from parent window */
			if C.xevent_xfocus(&ev).window != win {
				grabfocus()
			}
		case C.KeyPress:
			fmt.Println("KeyPress")
			keypress(C.xevent_xkey(&ev))
		case C.SelectionNotify:
			if C.xevent_xselection(&ev).property == utf8 {
				//TODO paste()
			}
		case C.VisibilityNotify:
			fmt.Println("VisibilityNotify")
			if C.xevent_xvisibility(&ev).state != C.VisibilityUnobscured {
				C.XRaiseWindow(dpy, win)
			}
		}
	}
}

func grabfocus() {
	var focuswin C.Window
	var revertwin C.int

	for i := 0; i < 100; i++ {
		C.XGetInputFocus(dpy, &focuswin, &revertwin)
		if focuswin == win {
			return
		}
		C.XSetInputFocus(dpy, win, C.RevertToParent, C.CurrentTime)
		time.Sleep(time.Millisecond)
	}
	panic("cannot grab focus")
}

func keypress(ev C.XKeyEvent) {
	var ksym C.KeySym = C.NoSymbol
	var status C.Status
	var buf [32]C.char
	len := C.XmbLookupString(xic, &ev, &buf[0], 32, &ksym, &status)
}
