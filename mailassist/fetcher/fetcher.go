package fetcher

import (
	"fmt"
	"log"
	"net/mail"
	"time"

	imap "github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
)

//Fetcher gets emails from a certain mailbox and a date
type Fetcher struct {
	client *client.Client
}

//New returns a new fetcher
func New(address, username, password string) (f *Fetcher, err error) {
	f = new(Fetcher)
	f.client, err = client.DialTLS(address, nil)
	if err != nil {
		return nil, err
	}
	if err = f.client.Login(username, password); err != nil {
		return nil, err
	}
	return
}

//Logout logouts from the IMAP server
func (f *Fetcher) Logout() error {
	return f.client.Logout()
}

//Mailboxes returns an array of mailboxes
func (f *Fetcher) Mailboxes() (mailboxes []string, err error) {
	mailboxesChan := make(chan *imap.MailboxInfo, 10)
	done := make(chan error, 1)
	go func() {
		done <- f.client.List("", "*", mailboxesChan)
	}()
	for m := range mailboxesChan {
		mailboxes = append(mailboxes, m.Name)
	}
	if err = <-done; err != nil {
		return nil, err
	}
	return
}

//Fetch returns an channel that will push fetched messages from an date
func (f *Fetcher) Fetch(mailboxName string, t time.Time) (chan *mail.Message, error) {
	err := f.selectMailbox(mailboxName)
	if err != nil {
		return nil, err
	}
	seqset, err := f.searchSince(t)
	if err != nil {
		return nil, err
	}
	emails := make(chan *imap.Message, 1)
	go func() {
		if err := f.client.Fetch(seqset, []string{"BODY[]"}, emails); err != nil {
			log.Fatalf("Error fetching messages: %s", err)
			close(emails)
		}
	}()

	messages := make(chan *mail.Message, 1)
	go func() {
		defer close(messages)
		for m := range emails {
			bodyReader := m.GetBody("BODY[]")
			if bodyReader == nil {
				log.Fatalln("Server didn't returned message body")
			}
			m, err := mail.ReadMessage(bodyReader)
			if err != nil {
				log.Fatal(err)
			}
			messages <- m
		}
	}()
	return messages, nil
}

func (f *Fetcher) selectMailbox(mailboxName string) error {
	_, err := f.client.Select(mailboxName, true)
	if err != nil {
		return fmt.Errorf("Cannot select mailbox '%s': %s", mailboxName, err)
	}
	return nil
}

func (f *Fetcher) searchSince(t time.Time) (*imap.SeqSet, error) {
	criteria := imap.NewSearchCriteria()
	criteria.Since = t
	seqNums, err := f.client.Search(criteria)
	if err != nil {
		return nil, fmt.Errorf("Search error: %s", err)
	}
	seqset := new(imap.SeqSet)
	seqset.AddNum(seqNums...)
	return seqset, nil
}
