package fetcher

import (
	"fmt"
	"os"
	"testing"
	"time"
)

var (
	USERNAME = os.Getenv("GMAIL_USERNAME")
	PASSWORD = os.Getenv("GMAIL_PASSWORD")
)

func TestLogin(t *testing.T) {
	f, err := New("imap.gmail.com:993", USERNAME, PASSWORD)
	if err != nil {
		t.Fatal(err)
	}
	if err = f.Logout(); err != nil {
		t.Error(err)
	}
	if err = f.Logout(); err == nil {
		t.Fatal(err)
	}
	_, err = New("imap.gmail.com:993", "", "")
	if err == nil {
		t.Error(err)
	}
}

func TestMailboxes(t *testing.T) {
	f, err := New("imap.gmail.com:993", USERNAME, PASSWORD)
	if err != nil {
		t.Fatal(err)
	}
	defer f.Logout()
	mailboxes, err := f.Mailboxes()
	if err != nil {
		t.Error(err)
	}
	mymailboxes := []string{
		"INBOX",
		"NWS",
		"[Gmail]",
		"[Gmail]/Dôležité",
		"[Gmail]/Kôš",
		"[Gmail]/Odoslané",
		"[Gmail]/Rozpísané správy",
		"[Gmail]/S hviezdičkou",
		"[Gmail]/Spam",
		"[Gmail]/Všetky správy",
	}
	if fmt.Sprint(mailboxes) != fmt.Sprint(mymailboxes) {
		t.Errorf("Not equal mailboxes %v", mailboxes)
	}
}

func TestSelectMailbox(t *testing.T) {
	f, err := New("imap.gmail.com:993", USERNAME, PASSWORD)
	if err != nil {
		t.Fatal(err)
	}
	if err = f.selectMailbox("[Gmail]/Všetky správy"); err != nil {
		t.Error(err)
	}
}

func TestFetch(t *testing.T) {
	f, err := New("imap.gmail.com:993", USERNAME, PASSWORD)
	if err != nil {
		t.Fatal(err)
	}
	defer f.Logout()
	messages, err := f.Fetch("[Gmail]/Všetky správy", time.Now().Add(-time.Hour*48))
	if err != nil {
		t.Fatal(err)
	}
	for m := range messages {
		fmt.Println(m.Header.Get("Subject"))
	}
}
