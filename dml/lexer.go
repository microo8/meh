package dml

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"sort"
	"strings"
	"unicode"
)

// Token represents a lexical token.
type Token int

//Token types
const (
	// Special tokens
	ILLEGAL Token = iota
	EOL
	EOF
	WS

	// Literals
	IDENT  // fields, table_name
	NUMBER // number
	COMMENT
	STRING

	// Misc characters
	COLON       // :
	COMMA       // ,
	CURLYBRACEL // {
	CURLYBRACER // }
	BRACKETL    // (
	BRACKETR    // )
	SQUAREL     // [
	SQUARER     // ]
	DOT         //.
	QUOT        //"
	SLASH       // /
	EQUALS      // =
	ASTERISK    // *

	// Keywords
	IMPORT
	AS
	VAR
	FUNC
	STYLE
	TEXT
	VALUE
)

//TokenNames maps tokens to it's names
var (
	TokenNames = map[Token]string{
		ILLEGAL:     "ILLEGAL",
		EOL:         "EOL",
		EOF:         "EOF",
		WS:          "WS",
		IDENT:       "IDENT",
		NUMBER:      "NUMBER",
		COMMENT:     "COMMENT",
		STRING:      "STRING",
		COLON:       "COLON",
		COMMA:       "COMMA",
		CURLYBRACEL: "CURLYBRACEL",
		CURLYBRACER: "CURLYBRACER",
		BRACKETL:    "BRACKETL",
		BRACKETR:    "BRACKETR",
		SQUAREL:     "SQUAREL",
		SQUARER:     "SQUARER",
		DOT:         "DOT",
		QUOT:        "QUOT",
		SLASH: "	SLASH",
		EQUALS:   "EQUALS",
		ASTERISK: "ASTERISK",
		IMPORT:   "IMPORT",
		AS:       "AS",
		VAR:      "VAR",
		FUNC:     "FUNC",
		STYLE:    "STYLE",
		TEXT:     "TEXT",
		VALUE:    "VALUE",
	}
	html5tags = sort.StringSlice{"a", "abbr", "acronym", "address", "applet", "area", "article", "aside", "audio", "b", "base", "basefont", "bdi", "bdo", "big", "blockquote", "body", "br", "button", "canvas", "caption", "center", "cite", "code", "col", "colgroup", "datalist", "dd", "del", "details", "dfn", "dialog", "dir", "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "font", "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hr", "html", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li", "link", "main", "map", "mark", "menu", "menuitem", "meta", "meter", "nav", "noframes", "noscript", "object", "ol", "optgroup", "option", "output", "p", "param", "pre", "progress", "q", "rp", "rt", "ruby", "s", "samp", "script", "section", "select", "small", "source", "span", "strike", "strong", "style", "sub", "summary", "sup", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "tt", "u", "ul", "var", "video", "wbr"}
	eof       = rune(0)
)

func isHTML5Tag(tag string) bool {
	return sort.SearchStrings(html5tags, strings.ToLower(tag)) != len(html5tags)
}

func isWhitespace(ch rune) bool {
	return ch == ' ' || ch == '\t' || ch == '\n'
}

// Scanner represents a lexical scanner.
type Scanner struct {
	r                  *bufio.Reader
	line, pos, prevpos int
}

// NewScanner returns a new instance of Scanner.
func NewScanner(r io.Reader) *Scanner {
	return &Scanner{r: bufio.NewReader(r)}
}

// read reads the next rune from the bufferred reader.
// Returns the rune(0) if an error occurs (or io.EOF is returned).
func (s *Scanner) read() rune {
	ch, _, err := s.r.ReadRune()
	if ch == '\n' {
		s.line++
		s.prevpos = s.pos
		s.pos = 0
	} else {
		s.pos++
		if err != nil {
			return eof
		}
	}
	return ch
}

// unread places the previously read rune back on the reader.
func (s *Scanner) unread() {
	s.r.UnreadRune()
	if s.pos == 0 {
		s.pos = s.prevpos
		if s.line == 0 {
			panic("Cannot unread! No runes readed")
		}
		s.line--
	} else {
		s.pos--
	}
}

//LinePos returns formated string of the line and position for error use
func (s *Scanner) LinePos() string {
	return fmt.Sprintf("line %d pos %d", s.line+1, s.pos)
}

func (s *Scanner) scanLine() (string, error) {
	var buf bytes.Buffer
	for {
		if ch := s.read(); ch == eof {
			return "", fmt.Errorf("Unexpected EOF at %s", s.LinePos())
		} else if ch == '\n' {
			return buf.String(), nil
		} else {
			buf.WriteRune(ch)
		}
	}
}

// scanWhitespace consumes the current rune and all contiguous whitespace.
func (s *Scanner) scanWhitespace() (tok Token, lit string) {
	// Create a buffer and read the current character into it.
	var buf bytes.Buffer
	buf.WriteRune(s.read())

	// Read every subsequent whitespace character into the buffer.
	// Non-whitespace characters and EOF will cause the loop to exit.
	for {
		if ch := s.read(); ch == eof {
			break
		} else if !isWhitespace(ch) {
			s.unread()
			break
		} else {
			buf.WriteRune(ch)
		}
	}

	return WS, buf.String()
}

func (s *Scanner) scanComment() (tok Token, lit string) {
	var buf bytes.Buffer
	buf.WriteRune(s.read())
	buf.WriteRune(s.read())

	switch buf.String() {
	case "//":
		for {
			if ch := s.read(); ch == eof || ch == '\n' {
				break
			} else {
				buf.WriteRune(ch)
			}
		}
		return COMMENT, buf.String()
	case "/*":
		for {
			if ch := s.read(); ch == eof {
				break
			} else if ch == '*' {
				buf.WriteRune(ch)
				if ch = s.read(); ch == '/' {
					buf.WriteRune(ch)
					break
				}
			} else {
				buf.WriteRune(ch)
			}
		}
		return COMMENT, buf.String()
	default:
		return ILLEGAL, buf.String()
	}
}

func (s *Scanner) scanString() (tok Token, lit string) {
	var buf bytes.Buffer
	buf.WriteRune(s.read())

	for {
		if ch := s.read(); ch == eof {
			break
		} else if ch == '"' {
			buf.WriteRune(ch)
			break
		} else {
			buf.WriteRune(ch)
		}
	}

	return STRING, buf.String()
}

// scanIdent consumes the current rune and all contiguous ident runes.
func (s *Scanner) scanIdent() (tok Token, lit string) {
	// Create a buffer and read the current character into it.
	var buf bytes.Buffer
	buf.WriteRune(s.read())

	// Read every subsequent ident character into the buffer.
	// Non-ident characters and EOF will cause the loop to exit.
	for {
		if ch := s.read(); ch == eof {
			break
		} else if !unicode.IsLetter(ch) && !unicode.IsDigit(ch) && ch != '_' {
			s.unread()
			break
		} else {
			buf.WriteRune(ch)
		}
	}

	// If the string matches a keyword then return that keyword.
	switch strings.ToUpper(buf.String()) {
	case "IMPORT":
		return IMPORT, buf.String()
	case "AS":
		return AS, buf.String()
	case "VAR":
		return VAR, buf.String()
	case "FUNC":
		return FUNC, buf.String()
	case "STYLE":
		return STYLE, buf.String()
	case "TEXT":
		return TEXT, buf.String()
	case "VALUE":
		return VALUE, buf.String()
	}

	// Otherwise return as a regular identifier.
	return IDENT, buf.String()
}

// Scan returns the next token and literal value.
func (s *Scanner) Scan() (tok Token, lit string) {
	// Read the next rune.
	ch := s.read()

	// If we see whitespace then consume all contiguous whitespace.
	// If we see a letter then consume as an ident or reserved word.
	if isWhitespace(ch) {
		s.unread()
		return s.scanWhitespace()
	} else if unicode.IsLetter(ch) {
		s.unread()
		return s.scanIdent()
	} else if ch == '/' {
		s.unread()
		return s.scanComment()
	} else if ch == '"' {
		s.unread()
		return s.scanString()
	}

	// Otherwise read the individual character.
	switch ch {
	case eof:
		return EOF, ""
	case '\n':
		return EOL, string(ch)

	case ':':
		return COLON, string(ch)
	case ',':
		return COMMA, string(ch)
	case '{':
		return CURLYBRACEL, string(ch)
	case '}':
		return CURLYBRACER, string(ch)
	case '(':
		return BRACKETL, string(ch)
	case ')':
		return BRACKETR, string(ch)
	case '[':
		return SQUAREL, string(ch)
	case ']':
		return SQUARER, string(ch)
	case '"':
		return QUOT, string(ch)
	case '/':
		return SLASH, string(ch)
	case '=':
		return EQUALS, string(ch)
	case '.':
		return DOT, string(ch)
	case '*':
		return ASTERISK, string(ch)
	}

	return ILLEGAL, string(ch)
}
