package dml

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
)

//Parse parser dml input from in and writes html to out
func Parse(in io.Reader, out io.Writer) error {
	p := NewParser(in)
	pack, err := p.Parse()
	if err != nil {
		return err
	}
	//TODO html around it and global variables and init()
	err = pack.Write(out)
	if err != nil {
		return err
	}
	return nil
}

// Parser represents a parser.
type Parser struct {
	s   *Scanner
	buf struct {
		tok Token  // last read token
		lit string // last read literal
		n   int    // buffer size (max=1)
	}
}

// NewParser returns a new instance of Parser.
func NewParser(r io.Reader) *Parser {
	return &Parser{s: NewScanner(r)}
}

// scan returns the next token from the underlying scanner.
// If a token has been unscanned then read that instead.
func (p *Parser) scan() (tok Token, lit string) {
	// If we have a token on the buffer, then return it.
	if p.buf.n != 0 {
		p.buf.n = 0
		return p.buf.tok, p.buf.lit
	}

	// Otherwise read the next token from the scanner.
	tok, lit = p.s.Scan()

	// Save it to the buffer in case we unscan later.
	p.buf.tok, p.buf.lit = tok, lit

	return
}

// unscan pushes the previously read token back onto the buffer.
func (p *Parser) unscan() { p.buf.n = 1 }

// scanIgnoreWhitespace scans the next non-whitespace token.
func (p *Parser) scanIgnoreWhitespace() (tok Token, lit string) {
	tok, lit = p.scan()
	if tok == WS {
		tok, lit = p.scan()
	}
	return
}

//scanExpectedToken scans another non-whitespace token, if the token is not the exptok returns formated error
func (p *Parser) scanExpectedToken(exptok Token, errorString string) (tok Token, lit string, err error) {
	if tok, lit = p.scanIgnoreWhitespace(); tok != exptok {
		return ILLEGAL, "", fmt.Errorf("Unexpected token %s (%s) at %s; expected %s %s", TokenNames[tok], lit, p.s.LinePos(), TokenNames[exptok], errorString)
	}
	return
}

func (p *Parser) scanStyle() (map[string]string, error) {
	style := make(map[string]string)
	tok, lit, err := p.scanExpectedToken(CURLYBRACEL, "as style beginning")
	if err != nil {
		return nil, err
	}
	for {
		tok, lit = p.scanIgnoreWhitespace()
		switch tok {
		case COMMENT:
		case IDENT:
			tok, _, err = p.scanExpectedToken(COLON, "after style parameter name")
			if err != nil {
				return nil, err
			}
			jssrc, err := p.s.scanLine()
			if err != nil {
				return nil, err
			}
			style[lit] = strings.TrimSpace(jssrc)
		case CURLYBRACER:
			return style, nil
		case EOF:
			return nil, fmt.Errorf("Unexpected EOF at %s while scanning style", p.s.LinePos())
		default:
			return nil, fmt.Errorf("Unexpected token %s at %s; expected IDENT or COMMENT in scanning style", TokenNames[tok], p.s.LinePos())
		}
	}
}

func (p *Parser) scanChild(pack *Package, node Node) (child *Child, err error) {
	child = new(Child)
	child.Parameters = make(map[string]string)
	child.Node = node
	_, _, err = p.scanExpectedToken(CURLYBRACEL, "after child name")
	if err != nil {
		return nil, err
	}

	for {
		tok, lit := p.scanIgnoreWhitespace()
		switch tok {
		case COMMENT:
		case IDENT: //parameter decl paramName: jssrc or child node
			tok, _ = p.scanIgnoreWhitespace()
			switch tok {
			case COLON: //parameter name
				var text string
				tok, text = p.scanIgnoreWhitespace()
				switch tok {
				case CURLYBRACEL: //multi row jssrc code
					p.s.unread()
					var buf bytes.Buffer
					for {
						if ch := p.s.read(); ch == '}' {
							buf.WriteRune(ch)
							break
						} else if ch == eof {
							return nil, fmt.Errorf("Unexpected EOF at %s while scanning multiline jssrc for parameter (%s) in child %s",
								p.s.LinePos(),
								lit, node.GetTag())
						} else {
							buf.WriteRune(ch)
						}
					}
					child.Parameters[lit] = strings.TrimSpace(buf.String())
				default: //one line jssrc expression
					var jssrc string
					jssrc, err = p.s.scanLine()
					if err != nil {
						return nil, err
					}
					child.Parameters[lit] = strings.TrimSpace(text + jssrc)
				}
			case CURLYBRACEL: //class or tag child in child
				//TODO find imported package identifier + scanChild must have the argument of Node, and this doesnt have to be here
				var childtmp Node
				if super, ok := pack.Classes[lit]; ok {
					p.unscan()
					childtmp, err = p.scanChild(pack, super)
					if err != nil {
						return nil, err
					}
				} else if isHTML5Tag(lit) {
					p.unscan()
					childtmp, err = p.scanChild(pack, &Tag{Tag: lit})
					if err != nil {
						return nil, err
					}
				} else {
					return nil, fmt.Errorf("%s is not class name or html5 tag at %s", lit, p.s.LinePos())
				}
				child.Children = append(child.Children, childtmp)
			case DOT: //child from an imported package in child
				var childtmp Node
				if importedPack, ok := pack.Imports[lit]; ok {
					tok, lit, err = p.scanExpectedToken(IDENT, "as class name from imported package "+importedPack.Ident)
					if err != nil {
						return nil, err
					}
					if super, ok := importedPack.Pack.Classes[lit]; ok {
						childtmp, err = p.scanChild(pack, super)
						if err != nil {
							return nil, err
						}
					} else {
						return nil, fmt.Errorf("%s is not an class from imported package %s at %s", lit, importedPack.Ident, p.s.LinePos())
					}
				} else {
					return nil, fmt.Errorf("%s is not an imported package identifiet at %s", lit, p.s.LinePos())
				}
				child.Children = append(child.Children, childtmp)
			}
		case STYLE:
			//style can be in class just onece
			if child.Style != nil {
				return nil, fmt.Errorf("Error at %s; child node %s already has style declared", p.s.LinePos(), node.GetTag())
			}
			child.Style, err = p.scanStyle()
			if err != nil {
				return nil, err
			}
		case TEXT: //text decl text: jssrc
			//TODO text can be in class/child just once
			tok, _, err = p.scanExpectedToken(COLON, "as text beginning")
			if err != nil {
				return nil, err
			}
			child.text, err = p.s.scanLine()
			if err != nil {
				return nil, err
			}
		case CURLYBRACER:
			return child, nil
		case EOF:
			return nil, fmt.Errorf("Unexpected EOF at %s while scanning class", p.s.LinePos())
		default:
			return nil, fmt.Errorf("Unexpected token %s (%s) at %s", TokenNames[tok], lit, p.s.LinePos())
		}
	}
}

func (p *Parser) scanClass(pack *Package, className, inherits string) (class *Class, err error) {
	class = new(Class)
	class.Parameters = make(map[string]string)
	class.Variables = make(map[string]*Variable)
	class.Functions = make(map[string]*Function)
	class.Name = className

	//Get inherited node
	tok, lit := p.scanIgnoreWhitespace()
	switch tok {
	case DOT:
		if imp, ok := pack.Imports[inherits]; ok {
			tok, lit, err = p.scanExpectedToken(IDENT, "as class from imported package "+inherits)
			if err != nil {
				return nil, err
			}
			if super, ok := imp.Pack.Classes[lit]; ok {
				class.Inherits = super
			} else {
				return nil, fmt.Errorf("Class %s not found in package %s at %s", lit, inherits, p.s.LinePos())
			}
		} else {
			return nil, fmt.Errorf("import identifier %s not found at %s", inherits, p.s.LinePos())
		}
	case CURLYBRACEL:
		if super, ok := pack.Classes[inherits]; ok { //it is a class
			class.Inherits = super
		} else if isHTML5Tag(inherits) { //it is a html5 tag
			class.Inherits = &Tag{Tag: inherits}
			break
		} else {
			return nil, fmt.Errorf("%s is not class name or html5 tag at %s", inherits, p.s.LinePos())
		}
	default:
		return nil, fmt.Errorf("Unexpected token %s at %s; expected CURLYBRACEL or DOT", TokenNames[tok], p.s.LinePos())
	}

	//scan class body
	for {
		tok, lit = p.scanIgnoreWhitespace()
		switch tok {
		case COMMENT:
		case IDENT: //parameter decl paramName: jssrc or child node
			tok, _ = p.scanIgnoreWhitespace()
			switch tok {
			case COLON: //parameter name
				var text string
				tok, text = p.scanIgnoreWhitespace()
				switch tok {
				case CURLYBRACEL: //multi line jssrc code
					p.s.unread()
					var buf bytes.Buffer
					for {
						if ch := p.s.read(); ch == '}' {
							buf.WriteRune(ch)
							break
						} else if ch == eof {
							return nil, fmt.Errorf("Unexpected EOF at %s while scanning multiline jssrc for parameter (%s) in class %s",
								p.s.LinePos(),
								lit, class.Name)
						} else {
							buf.WriteRune(ch)
						}
					}
					class.Parameters[lit] = strings.TrimSpace(buf.String())
				default: //one line jssrc expression
					var jssrc string
					jssrc, err = p.s.scanLine()
					if err != nil {
						return nil, err
					}
					class.Parameters[lit] = strings.TrimSpace(text + jssrc)
				}
			case CURLYBRACEL: //class or tag child
				var child Node
				if super, ok := pack.Classes[lit]; ok {
					p.unscan()
					child, err = p.scanChild(pack, super)
					if err != nil {
						return nil, err
					}
				} else if isHTML5Tag(lit) {
					p.unscan()
					child, err = p.scanChild(pack, &Tag{Tag: lit})
					if err != nil {
						return nil, err
					}
				} else {
					return nil, fmt.Errorf("%s is not class name or html5 tag at %s", lit, p.s.LinePos())
				}
				class.Children = append(class.Children, child)
			case DOT: //child from an imported package
				var child Node
				if importedPack, ok := pack.Imports[lit]; ok {
					tok, lit, err = p.scanExpectedToken(IDENT, "as class name from imported package "+importedPack.Ident)
					if err != nil {
						return nil, err
					}
					if super, ok := importedPack.Pack.Classes[lit]; ok {
						child, err = p.scanChild(pack, super)
						if err != nil {
							return nil, err
						}
					} else {
						return nil, fmt.Errorf("%s is not an class from imported package %s at %s", lit, importedPack.Ident, p.s.LinePos())
					}
				} else {
					return nil, fmt.Errorf("%s is not an imported package identifiet at %s", lit, p.s.LinePos())
				}
				class.Children = append(class.Children, child)
			default:
				return nil, fmt.Errorf("Unexpected token %s at %s; expected COLON or CURLYBRACEL in class definition", TokenNames[tok], p.s.LinePos())
			}
		case VAR: //variable decl var varName: jssrc
			tok, lit, err = p.scanExpectedToken(IDENT, "as variable name")
			if err != nil {
				return nil, err
			}
			variable := new(Variable)
			variable.Name = lit
			tok, _, err = p.scanExpectedToken(COLON, "after variable name")
			if err != nil {
				return nil, err
			}
			var jssrc string
			jssrc, err = p.s.scanLine()
			if err != nil {
				return nil, err
			}
			variable.Value = strings.TrimSpace(jssrc)
			class.Variables[variable.Name] = variable
		case FUNC: //func decl func funcName(self, *args) {funcBody}
			function := new(Function)
			//function name
			tok, lit, err = p.scanExpectedToken(IDENT, "as function name")
			if err != nil {
				return nil, err
			}
			function.Name = lit
			//function params begin
			tok, _, err = p.scanExpectedToken(BRACKETL, "as function params beginning")
			if err != nil {
				return nil, err
			}
			//self param required
			tok, lit, err = p.scanExpectedToken(IDENT, "as the required self parameter")
			if err != nil {
				return nil, err
			}
			function.Parameters = append(function.Parameters, lit)

			for {
				if tok, _ = p.scanIgnoreWhitespace(); tok == COMMA {
					tok, lit, err = p.scanExpectedToken(IDENT, "as function parameter")
					if err != nil {
						return nil, err
					}
					function.Parameters = append(function.Parameters, lit)
				} else if tok == BRACKETR { //function params end
					break
				} else {
					return nil, fmt.Errorf("Unexpected token %s at %s; expected COMMA or BRACKETR in function paramenters", TokenNames[tok], p.s.LinePos())
				}
			}

			//function body begin
			tok, _, err = p.scanExpectedToken(CURLYBRACEL, "as function body beginning")
			if err != nil {
				return nil, err
			}

			var buf bytes.Buffer
			for {
				if ch := p.s.read(); ch == '}' {
					break
				} else if ch == eof {
					return nil, fmt.Errorf("Unexpected EOF at %s while scanning function (%s) body in class", function.Name, p.s.LinePos())
				} else {
					buf.WriteRune(ch)
				}
			}
			function.Body = strings.TrimSpace(buf.String())
			class.Functions[function.Name] = function
		case STYLE:
			//style can be in class just onece
			if class.Style != nil {
				return nil, fmt.Errorf("Error at %s; class %s already has style declared", p.s.LinePos(), class.Name)
			}
			class.Style, err = p.scanStyle()
			if err != nil {
				return nil, err
			}
		case TEXT: //text decl text { jssrc }
			tok, _, err = p.scanExpectedToken(CURLYBRACEL, "as text beginning")
			if err != nil {
				return nil, err
			}
			class.text, err = p.s.scanLine()
			if err != nil {
				return nil, err
			}
		case CURLYBRACER:
			return class, nil
		case EOF:
			return nil, fmt.Errorf("Unexpected EOF at %s while scanning class", p.s.LinePos())
		default:
			return nil, fmt.Errorf("Unexpected token %s (%s) at %s", TokenNames[tok], lit, p.s.LinePos())
		}
	}
}

//Parse parses the input and returns Package
func (p *Parser) Parse() (pack *Package, err error) {
	pack = NewPackage()
	for {
		tok, lit := p.scanIgnoreWhitespace()
		switch tok {
		case COMMENT:
		case EOF:
			return nil, fmt.Errorf("Unexpected EOF at %s", p.s.LinePos())
		case ILLEGAL:
			return nil, fmt.Errorf("ILLEGAL token (%s) at %s", lit, p.s.LinePos())
		case IMPORT:
			tok, lit, err = p.scanExpectedToken(STRING, "as import path")
			if err != nil {
				return nil, err
			}
			imp := new(Import)
			imp.Path = lit
			tok, _, err = p.scanExpectedToken(AS, "after import path")
			if err != nil {
				return nil, err
			}
			tok, lit, err = p.scanExpectedToken(IDENT, "as imported package identifier")
			if err != nil {
				return nil, err
			}
			imp.Ident = lit
			var f *os.File
			f, err = os.Open(imp.Path[1:len(imp.Path)-1] + ".dml")
			if err != nil {
				return nil, fmt.Errorf("Cannot open %s on %d (%s)", imp.Path, p.buf.n, err)
			}
			parser := NewParser(f)
			var impPack *Package
			impPack, err = parser.Parse()
			if err != nil {
				return nil, fmt.Errorf("Error in importing file %s (%s) at %s", imp.Path, err, p.s.LinePos())
			}
			imp.Pack = impPack
			pack.Imports[imp.Ident] = imp
		case IDENT:
			className := lit
			tok, _, err = p.scanExpectedToken(COLON, "after class name x")
			if err != nil {
				return nil, err
			}
			tok, lit, err = p.scanExpectedToken(IDENT, "as inherits class/tag name")
			if err != nil {
				return nil, err
			}
			class, err := p.scanClass(pack, className, lit)
			if err != nil {
				return nil, err
			}
			pack.Classes[class.Name] = class
			for {
				if tok, _ = p.scanIgnoreWhitespace(); tok == EOF {
					return pack, nil
				} else if tok != COMMENT {
					p.unscan()
					break
				}
			}
		default:
			return nil, fmt.Errorf("Unexpected token %s (%s) at %s", TokenNames[tok], lit, p.s.LinePos())
		}
	}
}
