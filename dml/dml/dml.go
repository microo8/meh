package main

import (
	"flag"
	"log"
	"os"

	"github.com/microo8/meh/dml"
)

var (
	filename = flag.String("in", "main.dml", "the main file to parse")
	output   = flag.String("out", "main.html", "the output html file")
)

func main() {
	flag.Parse()
	log.SetFlags(log.Lshortfile | log.Ltime)

	in, err := os.Open(*filename)
	if err != nil {
		log.Fatalln(err)
	}
	defer in.Close()
	out, err := os.Create(*output)
	if err != nil {
		log.Fatalln(err)
	}
	defer out.Close()

	err = dml.Parse(in, out)
	if err != nil {
		log.Fatalln(err)
	}
}
