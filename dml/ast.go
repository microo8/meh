package dml

import (
	"io"
	"regexp"
	"text/template"
)

//Node is an interface which represents all nodes in dml file
type Node interface {
	GetTag() string
	GetParameters() map[string]string
	GetStyle() map[string]string
	GetChildren() []Node
	GetVariables() map[string]*Variable
	GetFunctions() map[string]*Function
}

//Element represents the html element properties
type Element struct {
	Parameters map[string]string
	Children   []Node
	Style      map[string]string
	text       string
}

//Tag represents the defautl html tag
type Tag struct {
	Element
	Tag string
}

//GetTag returns the tag type
func (t *Tag) GetTag() string {
	return t.Tag
}

//GetParameters returns the tag parameters
func (t *Tag) GetParameters() map[string]string {
	return t.Parameters
}

//GetChildren returns the child nodes
func (t *Tag) GetChildren() []Node {
	return t.Children
}

//GetVariables returns empty array because the default tag has no variables
func (t *Tag) GetVariables() map[string]*Variable {
	return nil
}

//GetFunctions returns empty array because the default tag has no functions
func (t *Tag) GetFunctions() map[string]*Function {
	return nil
}

//GetStyle returns style map
func (t *Tag) GetStyle() map[string]string {
	return t.Style
}

//Import represents the import statement
type Import struct {
	Path  string //the path to the dml file
	Ident string //the identifier for the imported dml file
	Pack  *Package
}

//Child is the main unit of the dml file, it represets the nodes which will be rendered to html
type Child struct {
	Element
	Node Node
}

//GetTag returns the tag type
func (c *Child) GetTag() string {
	return c.Node.GetTag()
}

//GetParameters returns the tag parameters
func (c *Child) GetParameters() map[string]string {
	params := make(map[string]string)
	for key, val := range c.Node.GetParameters() {
		params[key] = val
	}
	for key, val := range c.Parameters {
		params[key] = val
	}
	return params
}

//GetChildren returns the child nodes
func (c *Child) GetChildren() []Node {
	return append(c.Node.GetChildren(), c.Children...)
}

//GetVariables returns empty array because the default tag has no variables
func (c *Child) GetVariables() map[string]*Variable {
	return nil
}

//GetFunctions returns empty array because the default tag has no functions
func (c *Child) GetFunctions() map[string]*Function {
	return nil
}

//GetStyle returns style of the class
func (c *Child) GetStyle() map[string]string {
	style := make(map[string]string)
	for key, val := range c.Node.GetStyle() {
		style[key] = val
	}
	for key, val := range c.Style {
		style[key] = val
	}
	return style
}

//Class is the main unit of the dml file, it represets the nodes which will be rendered to html
type Class struct {
	Element
	Name      string
	Inherits  Node
	Variables map[string]*Variable
	Functions map[string]*Function
}

//GetTag returns the tag type
func (c *Class) GetTag() string {
	return c.Inherits.GetTag()
}

//GetParameters returns the tag parameters
func (c *Class) GetParameters() map[string]string {
	params := make(map[string]string)
	for key, val := range c.Inherits.GetParameters() {
		params[key] = val
	}
	for key, val := range c.Parameters {
		params[key] = val
	}
	return params
}

//GetChildren returns the child nodes
func (c *Class) GetChildren() []Node {
	return append(c.Inherits.GetChildren(), c.Children...)
}

//GetVariables returns empty array because the default tag has no variables
func (c *Class) GetVariables() map[string]*Variable {
	vars := make(map[string]*Variable)
	for key, val := range c.Inherits.GetVariables() {
		vars[key] = val
	}
	for key, val := range c.Variables {
		vars[key] = val
	}
	return vars
}

//GetFunctions returns empty array because the default tag has no functions
func (c *Class) GetFunctions() map[string]*Function {
	funcs := make(map[string]*Function)
	for key, val := range c.Inherits.GetFunctions() {
		funcs[key] = val
	}
	for key, val := range c.Functions {
		funcs[key] = val
	}
	return funcs
}

//GetStyle returns style of the class
func (c *Class) GetStyle() map[string]string {
	style := make(map[string]string)
	for key, val := range c.Inherits.GetStyle() {
		style[key] = val
	}
	for key, val := range c.Style {
		style[key] = val
	}
	return style
}

func (c *Class) Write(out io.Writer) error {
	funcMap := template.FuncMap{
		"ReplaceSelf": func(str string) string { return regexp.MustCompile("self.").ReplaceAllString(str, "___node.") },
	}
	t, err := template.New(c.Name).Funcs(funcMap).Parse(`function NewNode{{.Name}}() {
	var ___node = document.createElement('{{ .GetTag }}')
	//params
	{{ range $paramName, $paramValue := .GetParameters }}
	//TODO getter setter
	___node.{{ $paramName }} = eval({{ $paramValue }})
	{{ end }}

	//variables
	{{ range $key, $variable := .GetVariables }}
	//TODO getter setter global evaluated = [___node, "{{ $variable.Name }}", "{{ $variable.Value }}"]
	___node.{{ $variable.Name }} = eval("{{ $variable.Value }}")
	{{ end }}

	//functions
	{{ range $key, $function := .GetFunctions }}
	___node.{{ $function.Name }} = function({{ range $i, $param := $function.Parameters }}{{ if ne $i 0 }}{{ $param }}{{ end }}{{ end }}) {
		{{ $function.Body | ReplaceSelf }}
	}
	{{ end }}


	return ___node
}
`)
	if err != nil {
		return err
	}
	t.Execute(out, c)
	return nil
}

//Function is the inner function definition of the Class
type Function struct {
	Name       string
	Parameters []string
	Body       string
}

//Variable is the inner variable of the Class
type Variable struct {
	Name  string
	Value string
}

//Package represents the entire dml file
type Package struct {
	Imports map[string]*Import
	Classes map[string]*Class
}

//NewPackage returns an empty Package
func NewPackage() *Package {
	return &Package{
		Imports: make(map[string]*Import),
		Classes: make(map[string]*Class),
	}
}

//Write exports html to out if the Package contains main class
func (p *Package) Write(out io.Writer) (err error) {
	for _, pack := range p.Imports {
		err = pack.Pack.Write(out)
		if err != nil {
			return err
		}
	}
	for _, class := range p.Classes {
		err = class.Write(out)
		if err != nil {
			return err
		}
	}
	return nil
}
