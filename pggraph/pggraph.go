package main

import (
	"database/sql"
	"flag"
	"fmt"
	"os"

	_ "github.com/lib/pq"
)

//go run pggraph.go -db root:nwspass@votum_server:5432/votum | dot -Tsvg -oout.svg

var (
	dbConn   = flag.String("db", "postgres:password@localhost:5432/postgres", `database connection string`)
	schema   = flag.String("s", "public", `database schema`)
	filename = flag.String("o", "", "output dot file (stdout if empty)")

	db               *sql.DB
	getTables        *sql.Stmt
	getColumns       *sql.Stmt
	getPK            *sql.Stmt
	getFK            *sql.Stmt
	getFKColumnsFrom *sql.Stmt
	getFKTo          *sql.Stmt
)

func init() {
	flag.Parse()
	var err error
	db, err = sql.Open("postgres", "postgres://"+*dbConn)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s", err)
		return
	}
	if err = db.Ping(); err != nil {
		fmt.Fprintf(os.Stderr, "connection error: %s", err)
		os.Exit(1)
	}
	getTables, err = db.Prepare(`SELECT table_schema, table_name FROM information_schema.tables
	WHERE table_schema = $1 AND table_type = 'BASE TABLE'`)
	if err != nil {
		fmt.Fprintf(os.Stderr, "connection error: %s", err)
		os.Exit(1)
	}
	getColumns, err = db.Prepare(`SELECT column_name, COALESCE(domain_name, data_type) AS data_type, is_nullable='YES'
			FROM information_schema.columns
            WHERE table_schema = $1 AND table_name = $2 ORDER BY ordinal_position`)
	if err != nil {
		fmt.Fprintf(os.Stderr, "connection error: %s", err)
		os.Exit(1)
	}
	getPK, err = db.Prepare(`SELECT column_name FROM information_schema.constraint_column_usage
            WHERE constraint_name = (SELECT constraint_name FROM information_schema.table_constraints
            WHERE table_schema = $1 AND table_name = $2 AND constraint_type = 'PRIMARY KEY')`)
	if err != nil {
		fmt.Fprintf(os.Stderr, "connection error: %s", err)
		os.Exit(1)
	}
	getFK, err = db.Prepare(`SELECT distinct constraint_name FROM information_schema.table_constraints
                WHERE table_schema = $1 AND table_name = $2 AND constraint_type = 'FOREIGN KEY'`)
	if err != nil {
		fmt.Fprintf(os.Stderr, "connection error: %s", err)
		os.Exit(1)
	}
	getFKColumnsFrom, err = db.Prepare(`SELECT distinct column_name FROM information_schema.key_column_usage
                WHERE constraint_name = $1`)
	if err != nil {
		fmt.Fprintf(os.Stderr, "connection error: %s", err)
		os.Exit(1)
	}
	getFKTo, err = db.Prepare(`SELECT distinct table_schema, table_name, column_name FROM information_schema.constraint_column_usage WHERE constraint_name = $1`)
	if err != nil {
		fmt.Fprintf(os.Stderr, "connection error: %s", err)
		os.Exit(1)
	}
}

func main() {
	defer db.Close()
	tables, err := newTables()
	if err != nil {
		fmt.Fprintf(os.Stderr, "table selection error: %s", err)
		return
	}
	out := os.Stdout
	if *filename != "" {
		out, err = os.Create(*filename)
		if err != nil {
			fmt.Fprintf(os.Stderr, "output file creation error %s", err)
			return
		}
		defer out.Close()
	}
	out.WriteString("digraph " + *schema + " {\n")
	defer out.WriteString("}")
	for _, table := range tables {
		table.Write(out)
	}
	for _, table := range tables {
		table.WriteEdges(out)
	}
}

func newTables() ([]*table, error) {
	tableRows, err := getTables.Query(*schema)
	if err != nil {
		return nil, err
	}
	defer tableRows.Close()
	var tables []*table
	for tableRows.Next() {
		var s, n string
		if err = tableRows.Scan(&s, &n); err != nil {
			return nil, err
		}
		t, err := newTable(s, n)
		if err != nil {
			return nil, err
		}
		tables = append(tables, t)
	}
	return tables, nil
}

type table struct {
	Schema      string
	Name        string
	Columns     []column
	PrimaryKeys []string
	ForeignKeys []foreignKey
}

func (t *table) Write(out *os.File) {
	out.WriteString(t.Name + " [shape=none;label=<<table border=\"0\" cellspacing=\"0\">")
	defer out.WriteString("</table>>;];\n")
	out.WriteString("<tr><td bgcolor=\"#3f51b5\" color=\"white\">" + t.Name + "</td></tr>")
	for _, col := range t.Columns {
		out.WriteString("<tr><td port=\"" + col.Name + "\" border=\"1\">")
		if t.isPK(col.Name) {
			out.WriteString("⚷ ")
		}
		out.WriteString(col.Name + ":<b>" + col.Type + "</b>")
		//if !col.Nullable {
		//	out.WriteString("[NOTNULL]")
		//}
		out.WriteString("</td></tr>")
	}
}

func (t *table) WriteEdges(out *os.File) {
	for _, fk := range t.ForeignKeys {
		for _, fromCol := range fk.FromColumns {
			for _, toCol := range fk.ToColumn {
				out.WriteString(t.Name + ":" + fromCol + " -> " + toCol.TableName + ":" + toCol.ColumnName + ";\n")
			}
		}
	}
}

func (t *table) isPK(col string) bool {
	for _, c := range t.PrimaryKeys {
		if c == col {
			return true
		}
	}
	return false
}

func newTable(schema, name string) (*table, error) {
	t := &table{Schema: schema, Name: name}
	//get columns
	columnRows, err := getColumns.Query(t.Schema, t.Name)
	if err != nil {
		return nil, err
	}
	defer columnRows.Close()
	for columnRows.Next() {
		var c column
		if err = columnRows.Scan(&c.Name, &c.Type, &c.Nullable); err != nil {
			return nil, err
		}
		t.Columns = append(t.Columns, c)
	}
	//get primary keys
	pkRows, err := getPK.Query(t.Schema, t.Name)
	if err != nil {
		return nil, err
	}
	defer pkRows.Close()
	for pkRows.Next() {
		var pkName string
		if err = pkRows.Scan(&pkName); err != nil {
			return nil, err
		}
		t.PrimaryKeys = append(t.PrimaryKeys, pkName)
	}
	//get foreign keys
	fkRows, err := getFK.Query(t.Schema, t.Name)
	if err != nil {
		return nil, err
	}
	defer fkRows.Close()
	for fkRows.Next() {
		var fkName string
		if err = fkRows.Scan(&fkName); err != nil {
			return nil, err
		}
		fromColsRows, err := getFKColumnsFrom.Query(fkName)
		if err != nil {
			return nil, err
		}
		defer fromColsRows.Close()
		var fk foreignKey
		for fromColsRows.Next() {
			var fromCol string
			if err = fromColsRows.Scan(&fromCol); err != nil {
				return nil, err
			}
			fk.FromColumns = append(fk.FromColumns, fromCol)
		}
		fkToRows, err := getFKTo.Query(fkName)
		if err != nil {
			return nil, err
		}
		defer fkToRows.Close()
		for fkToRows.Next() {
			var tc toColumn
			if err = fkToRows.Scan(&tc.Schema, &tc.TableName, &tc.ColumnName); err != nil {
				return nil, err
			}
			fk.ToColumn = append(fk.ToColumn, tc)
		}
		t.ForeignKeys = append(t.ForeignKeys, fk)
	}
	return t, nil
}

type column struct {
	Name     string
	Type     string
	Nullable bool
}

type foreignKey struct {
	FromColumns []string
	ToColumn    []toColumn
}

type toColumn struct {
	Schema     string
	TableName  string
	ColumnName string
}
