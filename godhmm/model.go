package godhmm

import (
	"encoding/gob"
	"log"
	"math"
	"math/rand"
	"os"
	"time"
)

const EXPMIN float64 = -708.3
const LOGMIN float64 = 2.45E-308

func godhmmExp(x float64) float64 {
	if x < EXPMIN {
		return math.Exp(EXPMIN)
	} else {
		return math.Exp(x)
	}
}

func godhmmLog(x float64) float64 {
	if x < LOGMIN {
		return math.Log(LOGMIN)
	} else {
		return math.Log(x)
	}
}

func logsumexp(xs []float64) (ret float64) {
	xmax := -math.MaxFloat64
	for _, x := range xs {
		xmax = math.Max(xmax, x)
	}
	for _, x := range xs {
		ret += godhmmExp(x - xmax)
	}
	ret = xmax + godhmmLog(ret)
	if ret == math.NaN() {
		log.Fatal("NaN!", xs, xmax)
	}
	return ret
}

//Model represents the discrete hidden markov model
type Model struct {
	InitialTransition []float64
	TransitionMatrix  [][]float64
	StateHistograms   []map[rune]float64
}

//NewModel creates new DHMM
//n - is the number of states
func New(n int) *Model {
	//TODO n>2
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	model := new(Model)

	//init InitialTransition
	model.InitialTransition = make([]float64, n-2)
	for i := range model.InitialTransition {
		model.InitialTransition[i] = godhmmLog(r.Float64() * 1e-300)
	}

	//init TransitionMatrix, last row is the end state
	model.TransitionMatrix = make([][]float64, n-2) //from the S and E cannot transit
	for i := range model.TransitionMatrix {
		model.TransitionMatrix[i] = make([]float64, n-1) //to the S cannot transit, but to E you can
		for j := range model.TransitionMatrix[i] {
			model.TransitionMatrix[i][j] = godhmmLog(r.Float64() * 1e-300)
		}
	}

	//init StateHistograms
	model.StateHistograms = make([]map[rune]float64, n-2) //S and E don't emit
	for i := range model.StateHistograms {
		model.StateHistograms[i] = make(map[rune]float64)
	}
	return model
}

func Load(filename string) (*Model, error) {
	var model *Model
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	dec := gob.NewDecoder(file)
	err = dec.Decode(&model)
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (model *Model) Save(filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	enc := gob.NewEncoder(file)
	err = enc.Encode(model)
	if err != nil {
		return err
	}
	return nil
}

//N returns number of states in the Model, including the start and end states
func (model *Model) N() int {
	return len(model.TransitionMatrix)
}

//Train trains the model from array of sequences for one iteration
func (model *Model) Train(sequence string) (prop float64) {
	random := rand.New(rand.NewSource(time.Now().UnixNano())) //TODO
	runeSequence := []rune(sequence)
	sequenceLength := len(runeSequence)

	for _, r := range runeSequence {
		if _, ok := model.StateHistograms[0][r]; !ok {
			for _, stateHistogram := range model.StateHistograms {
				stateHistogram[r] = math.Log(random.Float64() * 1e-300)
			}
		}
	}

	alphachan := make(chan [][]float64)
	go func() {
		//forward
		tmpsum := make([]float64, len(model.StateHistograms))
		alpha := make([][]float64, len(model.StateHistograms))
		for i := range alpha {
			alpha[i] = make([]float64, sequenceLength)
		}
		for i := range alpha {
			alpha[i][0] = model.InitialTransition[i] + model.StateHistograms[i][runeSequence[0]]
		}
		for t := 0; t < sequenceLength-1; t++ {
			for j := range model.StateHistograms {
				for i := range model.StateHistograms {
					tmpsum[i] = alpha[i][t] + model.TransitionMatrix[i][j]
				}
				alpha[j][t+1] = model.StateHistograms[j][runeSequence[t+1]] + logsumexp(tmpsum)
			}
		}
		alphachan <- alpha
	}()

	betachan := make(chan [][]float64)
	go func() {
		//backward
		tmpsum := make([]float64, len(model.StateHistograms))
		beta := make([][]float64, len(model.StateHistograms))
		for i := range model.StateHistograms {
			beta[i] = make([]float64, sequenceLength)
		}
		//first beta_i(T) = 1 is ok, log(1) = 0 and that is the zero value
		for t := sequenceLength - 2; t >= 0; t-- {
			for i := range model.StateHistograms {
				for j := range model.StateHistograms {
					tmpsum[j] = beta[j][t+1] + model.TransitionMatrix[i][j] + model.StateHistograms[j][runeSequence[t+1]]
				}
				beta[i][t] = logsumexp(tmpsum)
			}
		}
		betachan <- beta
	}()

	alpha := <-alphachan
	beta := <-betachan

	gamachan := make(chan [][]float64)
	go func() {
		//update
		tmpsum := make([]float64, len(model.StateHistograms))
		gama := make([][]float64, len(model.StateHistograms))
		for i := range gama {
			gama[i] = make([]float64, sequenceLength)
		}
		for t := range runeSequence {
			for j := range model.StateHistograms {
				tmpsum[j] = alpha[j][t] + beta[j][t]
			}
			alphabeta := logsumexp(tmpsum)
			for i := range gama {
				gama[i][t] = alpha[i][t] + beta[i][t] - alphabeta
			}
		}
		gamachan <- gama
	}()

	xichan := make(chan [][][]float64)
	go func() {
		tmpsum := make([]float64, len(model.StateHistograms))
		xi := make([][][]float64, len(model.StateHistograms))
		for i := range model.StateHistograms {
			xi[i] = make([][]float64, len(model.StateHistograms))
			for j := range model.StateHistograms {
				xi[i][j] = make([]float64, sequenceLength)
			}
		}
		for t := 0; t < sequenceLength-1; t++ {
			tmptmpsum := make([]float64, len(model.StateHistograms))
			for i := range model.StateHistograms {
				for j := range model.StateHistograms {
					tmptmpsum[j] = alpha[i][t] + model.TransitionMatrix[i][j] + beta[j][t+1] + model.StateHistograms[j][runeSequence[t+1]]
				}
				tmpsum[i] = logsumexp(tmptmpsum)
			}
			sum := logsumexp(tmpsum)
			for i := range model.StateHistograms {
				for j := range model.StateHistograms {
					xi[i][j][t] = alpha[i][t] + model.TransitionMatrix[i][j] + beta[j][t+1] + model.StateHistograms[j][runeSequence[t+1]] - sum
				}
			}
		}
		xichan <- xi
	}()

	gama := <-gamachan
	//log.Print("GAMA", gama)
	//update InitialTransition
	for i := range model.StateHistograms {
		model.InitialTransition[i] = gama[i][0]
	}
	xi := <-xichan

	//update TransitionMatrix
	for i := range model.StateHistograms {
		sumgama := logsumexp(gama[i][:len(gama[i])-1])
		for j := range model.StateHistograms {
			model.TransitionMatrix[i][j] = logsumexp(xi[i][j][:len(xi[i][j])-1]) - sumgama
			//if transition from i to j is below treshold, then cancel the path
			if model.TransitionMatrix[i][j] < -23.025850929940457 {
				model.TransitionMatrix[i][j] = math.Inf(-1)
			}
		}
		model.TransitionMatrix[i][len(model.StateHistograms)] = gama[i][sequenceLength-1] - logsumexp(gama[i])
		if model.TransitionMatrix[i][len(model.StateHistograms)] < -23.025850929940457 {
			model.TransitionMatrix[i][len(model.StateHistograms)] = math.Inf(-1)
		}
	}

	//update StateHistograms
	for k, _ := range model.StateHistograms[0] {
		for i := range model.StateHistograms {
			var tmpgama []float64
			for t := range runeSequence {
				if k == runeSequence[t] {
					tmpgama = append(tmpgama, gama[i][t])
				}
			}
			if len(tmpgama) > 0 {
				newHist := logsumexp(tmpgama) - logsumexp(gama[i])
				prop += math.Abs(godhmmExp(model.StateHistograms[i][k]) - godhmmExp(newHist))
				model.StateHistograms[i][k] = newHist
			}
		}
	}
	/*
		log.Println("OBSERVABLES: ", len(model.StateHistograms[0]), " - ")
		for r := range model.StateHistograms[0] {
			print(string(r))
		}
		println()
	*/

	return prop
}

//Viterbi returns the log probability that the model will generate the sequence
func (model *Model) Viterbi(sequence string) float64 {
	return 1
}

//Generate generates an random sequence
func (model *Model) Generate(maxLength int) string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	var runeSequence []rune
	//begin from the start state
	actualState := 0
	maxInitTrans := -math.MaxFloat64
	for i, trans := range model.InitialTransition {
		initTrans := godhmmLog(r.Float64()*1e-300) + trans
		if initTrans > maxInitTrans {
			maxInitTrans = initTrans
			actualState = i
		}
	}
	//while not in the end state
	for actualState < model.N()-1 {
		//not emit when in start or end state
		var actualObservable rune
		maxEmitProp := -math.MaxFloat64
		for i, _ := range model.StateHistograms[actualState] {
			emitProp := godhmmLog(r.Float64()*1e-300) + model.StateHistograms[actualState][i]
			if emitProp > maxEmitProp {
				maxEmitProp = emitProp
				actualObservable = i
			}
		}
		runeSequence = append(runeSequence, actualObservable)
		if len(runeSequence) >= maxLength {
			break
		}

		nextState := 0
		maxTransProp := -math.MaxFloat64
		for i := 0; i < model.N()+1; i++ {
			transProp := godhmmLog(r.Float64()*1e-300) + model.TransitionMatrix[actualState][i]
			if transProp > maxTransProp {
				maxTransProp = transProp
				nextState = i
			}
		}
		actualState = nextState
	}
	return string(runeSequence)
}
