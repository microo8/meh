package godhmm

import (
	"testing"
)

func TestModel(t *testing.T) {
	N := 20
	model := New(N)
	prop := 100.0
	for prop > 1e-5 {
		model.Train("fooxy")
		model.Train("barxy")
		prop = model.Train("mehxy")
		println(prop)
	}
	for i := 0; i < 10; i++ {
		println(model.Generate())
	}
}

func TestSave(t *testing.T) {
	model := New(5)
	err := model.Save("model.json")
	if err != nil {
		t.Error(err)
	}

	model2, err := Load("model.json")
	if err != nil {
		t.Error(err)
	}
	if len(model2.TransitionMatrix) != len(model.TransitionMatrix) {
		t.Error("model transitionMatrix not equal")
	}
}
