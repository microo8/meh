package main

import (
	"fmt"
	"log"
	"time"

	"github.com/gizak/termui"
	"github.com/microo8/meh/cexio"
)

var (
	ohlcvs          []cexio.OHLCV
	ohlcvsLow       []float64
	ohlcvsTimestamp []string
	timeDiff        time.Duration
	timeWindow      = time.Minute * 120

	lc0   *termui.LineChart
	parCS *termui.Par
)

func main() {
	c, err := cexio.Connect(apiKey, apiSecret)
	if err != nil {
		log.Fatalf("error connecting and authentificating (%s)", err)
	}
	defer c.Close()
	if err := termui.Init(); err != nil {
		panic(err)
	}
	defer termui.Close()

	lc0 = termui.NewLineChart()
	lc0.BorderLabel = "BTC/USD OHLCV"
	lc0.AxesColor = termui.ColorWhite
	lc0.LineColor = termui.ColorGreen | termui.AttrBold
	lc0.Height = termui.TermHeight() - 5

	parActual := termui.NewPar("")
	parActual.Height = 5
	parActual.BorderLabel = "Actual BTC/USD"
	parActual.BorderFg = termui.ColorMagenta

	parCS = termui.NewPar("")
	parCS.Height = 5
	parCS.BorderLabel = "Chart settings"
	parCS.BorderFg = termui.ColorMagenta
	refreshCS()

	go func() {
		for ohlcv := range c.OHLCV("BTC", "USD", "1m") {
			ohlcvs = append(ohlcvs, ohlcv)
			ohlcvsLow = append(ohlcvsLow, ohlcv.Low)
			ohlcvsTimestamp = append(ohlcvsTimestamp, time.Time(ohlcv.Timestamp).Format("060102|15:04"))

			refreshLC()
			parActual.Text = fmt.Sprintf(
				"Open: %.2f Close: %.2f\nLow: [%.2f](fg-green) High: [%.2f](fg-red)\nVolume: [%d](fg-bold)",
				ohlcv.Open, ohlcv.Close, ohlcv.Low, ohlcv.High, ohlcv.Volume,
			)
			termui.Body.Align()
			termui.Render(termui.Body)
		}
	}()

	termui.Body.AddRows(
		termui.NewRow(
			termui.NewCol(12, 0, lc0),
		),
		termui.NewRow(
			termui.NewCol(5, 0, parActual),
			termui.NewCol(7, 0, parCS),
		),
	)

	termui.Body.Align()
	termui.Render(termui.Body)
	termui.Handle("/sys/wnd/resize", func(e termui.Event) {
		termui.Body.Width = termui.TermWidth()
		lc0.Height = termui.TermHeight() - 5
		termui.Body.Align()
		termui.Render(termui.Body)
	})
	termui.Handle("/sys/kbd/q", func(termui.Event) {
		termui.StopLoop()
	})
	termui.Handle("/sys/kbd/j", func(termui.Event) {
		timeWindow -= time.Minute
		refreshCS()
	})
	termui.Handle("/sys/kbd/k", func(termui.Event) {
		timeWindow += time.Minute
		refreshCS()
	})
	termui.Handle("/sys/kbd/h", func(termui.Event) {
		if timeDiff == 0 {
			return
		}
		timeDiff -= time.Minute
		refreshCS()
	})
	termui.Handle("/sys/kbd/l", func(termui.Event) {
		timeDiff += time.Minute
		refreshCS()
	})
	termui.Handle("/timer/1s", func(e termui.Event) {
		refreshCS()
	})

	termui.Loop()
}

func refreshCS() {
	parCS.Text = fmt.Sprintf(
		"Window: [%s](fg-bold)\nActual Time: [%s](fg-magenta)",
		timeWindow.String(),
		time.Now().Add(-timeDiff).Format("2006-01-02 15:04:05"),
	)
	refreshLC()
	termui.Body.Align()
	termui.Render(termui.Body)
}

func refreshLC() {
	actualTime := time.Now()
	from := actualTime.Add(-timeWindow - timeDiff)
	to := actualTime.Add(-timeDiff)
	lc0.Data = nil
	lc0.DataLabels = nil
	fromIndex := 0
	toIndex := len(ohlcvs) - 1
	for i := 0; i < len(ohlcvs); i++ {
		if from.After(time.Time(ohlcvs[i].Timestamp)) {
			continue
		}
		fromIndex = i
		break
	}
	for i := len(ohlcvs) - 1; i >= 0; i-- {
		if to.Before(time.Time(ohlcvs[i].Timestamp)) {
			continue
		}
		toIndex = i
		break
	}

	lc0.Data = ohlcvsLow[fromIndex : toIndex+1]
	lc0.DataLabels = ohlcvsTimestamp[fromIndex : toIndex+1]
}
