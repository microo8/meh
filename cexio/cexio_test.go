package cexio

import (
	"log"
	"testing"
)

func TestSignature(t *testing.T) {
	data := []struct {
		key, secret, signature string
		timestamp              int64
	}{
		{
			secret:    "1IuUeW4IEWatK87zBTENHj1T17s",
			timestamp: 1448034533,
			key:       "1WZbtMTbMbo2NsW12vOz9IuPM",
			signature: "7d581adb01ad22f1ed38e1159a7f08ac5d83906ae1a42fe17e7d977786fe9694",
		},
		{
			secret:    "1IuUeW4IEWatK87zBTENHj1T17s",
			timestamp: 1448035135,
			key:       "1WZbtMTbMbo2NsW12vOz9IuPM",
			signature: "9a84b70f51ea2b149e71ef2436752a1a7c514f521e886700bcadd88f1767b7db",
		},
	}
	for _, d := range data {
		signature := createSignature(d.key, d.secret, d.timestamp)
		if signature != d.signature {
			t.Errorf("wrong signature calculated, must be: (%s) got (%s)", d.signature, signature)
		}
	}
}

func TestConnection(t *testing.T) {
	c, err := Connect(apiKey, apiSecret)
	if err != nil {
		t.Fatalf("error connecting and authentificating (%s)", err)
	}
	err = c.Close()
	if err != nil {
		t.Fatalf("error disconnecting (%s)", err)
	}
}

func TestTicker(t *testing.T) {
	c, err := Connect(apiKey, apiSecret)
	if err != nil {
		t.Fatalf("error connecting and authentificating (%s)", err)
	}
	defer c.Close()
	c.Ticker()
	/*
		for tick := range c.Ticker() {
			log.Printf("%v\n", tick)
		}
	*/
}

func TestOrderBook(t *testing.T) {
	c, err := Connect(apiKey, apiSecret)
	if err != nil {
		t.Fatalf("error connecting and authentificating (%s)", err)
	}
	defer c.Close()
	/*
		for ob := range c.OrderBook("ETH", "EUR") {
			t := time.Time(ob.Timestamp)
			log.Printf("%s %v\n", t.Format(time.UnixDate), ob)
		}
	*/
}

func TestOHLCV(t *testing.T) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	c, err := Connect(apiKey, apiSecret)
	if err != nil {
		t.Fatalf("error connecting and authentificating (%s)", err)
	}
	defer c.Close()
	for ohlcv := range c.OHLCV("BTC", "USD", "1m") {
		log.Println(ohlcv)
	}
}
