package cexio

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"
	"sync/atomic"
	"time"
)

var (
	reqCounter uint64
)

type msg struct {
	data []byte
	err  error
}

type wsMsg struct {
	E    string          `json:"e"`
	OID  string          `json:"oid,omitempty"`
	OK   string          `json:"ok,omitempty"`
	Pair string          `json:"pair,omitempty"`
	Data json.RawMessage `json:"data"`
}

func wrapMsg(e string, i interface{}) ([]byte, error) {
	var wsm wsMsg
	wsm.E = e
	wsm.OID = fmt.Sprintf("%#x", atomic.AddUint64(&reqCounter, 1))
	data, err := json.Marshal(i)
	if err != nil {
		return nil, err
	}
	wsm.Data = data
	return json.Marshal(wsm)
}

type disconnectingMsg struct {
	Reason string `json:"reason"`
	Time   int64  `json:"time"`
}

type authResponse struct {
	OK    string `json:"ok"`
	Error string `json:"error"`
}

//authRequest is the authorization request
type authRequest struct {
	wsMsg
	Auth struct {
		Key       string `json:"key"`
		Signature string `json:"signature"`
		Timestamp int64  `json:"timestamp"`
	} `json:"auth"`
}

func createSignature(key, secret string, timestamp int64) string {
	str := strconv.FormatInt(timestamp, 10) + key
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))

}

//newAuthRequest creates an authorization request from the key and secret
func newAuthRequest(key, secret string) *authRequest {
	timestamp := time.Now().Unix()
	var authReq authRequest
	authReq.OID = "auth"
	authReq.E = "auth"
	authReq.Auth.Key = key
	authReq.Auth.Signature = createSignature(key, secret, timestamp)
	authReq.Auth.Timestamp = timestamp
	return &authReq
}

//tickerMsg is used for subscription to ticker
type tickerMsg struct {
	wsMsg
	Rooms []string `json:"rooms"`
}

//TickMsg ticking response send from server
//Ticker subscription allows you to subscribe to real-time updates of executed trades.
//Notification does not include information like lowest and highest prices, volume, but indicates that trading is happening.
type TickMsg struct {
	Symbol1 string `json:"symbol1"`
	Symbol2 string `json:"symbol2"`
	Price   string `json:"price"`
}

//reqOrderBook is the order book subscription request
type reqOrderBook struct {
	Pair      []string `json:"pair"`
	Subscribe bool     `json:"subscribe"`
	Depth     int      `json:"depth"`
}

//OrderBook the order book
type OrderBook struct {
	Timestamp timestamp   `json:"timestamp"`
	Time      timestamp   `json:"time"`
	Bids      [][]float64 `json:"bids"`
	Asks      [][]float64 `json:"asks"`
	Pair      string      `json:"pair"`
	ID        int64       `json:"id"`
}

type timestamp time.Time

func (t *timestamp) UnmarshalJSON(b []byte) error {
	ts, err := strconv.Atoi(string(b))
	if err != nil {
		ts, err = strconv.Atoi(string(b[1 : len(b)-1]))
		if err != nil {
			return err
		}
	}
	*t = timestamp(time.Unix(int64(ts), 0))
	return nil
}

type reqOHLCV struct {
	E     string   `json:"e"`
	I     string   `json:"i"`
	Rooms []string `json:"rooms"`
}

//OHLCV ...
type OHLCV struct {
	Pair      string    `json:"pair"`
	Timestamp timestamp `json:"time"`
	Open      float64   `json:"o"`
	High      float64   `json:"h"`
	Low       float64   `json:"l"`
	Close     float64   `json:"c"`
	Volume    int64     `json:"v"`
	D         int64     `json:"d"`
}

//UnmarshalJSON ...
func (ohlcv *OHLCV) UnmarshalJSON(b []byte) error {
	rawMap := make(map[string]json.RawMessage)
	if err := json.Unmarshal(b, &rawMap); err != nil {
		return err
	}
	ohlcv.Pair = string(rawMap["pair"][1 : len(rawMap["pair"])-1])
	ts, err := strconv.Atoi(string(rawMap["time"][1 : len(rawMap["time"])-1]))
	if err != nil {
		return err
	}
	ohlcv.Timestamp = timestamp(time.Unix(int64(ts), 0))
	v := string(rawMap["o"][1 : len(rawMap["o"])-1])
	if ohlcv.Open, err = strconv.ParseFloat(v, 64); err != nil {
		return err
	}
	v = string(rawMap["c"][1 : len(rawMap["c"])-1])
	if ohlcv.Close, err = strconv.ParseFloat(v, 64); err != nil {
		return err
	}
	v = string(rawMap["h"][1 : len(rawMap["h"])-1])
	if ohlcv.High, err = strconv.ParseFloat(v, 64); err != nil {
		return err
	}
	v = string(rawMap["l"][1 : len(rawMap["l"])-1])
	if ohlcv.Low, err = strconv.ParseFloat(v, 64); err != nil {
		return err
	}
	v = string(rawMap["v"][1 : len(rawMap["v"])-1])
	if ohlcv.Volume, err = strconv.ParseInt(v, 10, 64); err != nil {
		return err
	}
	v = string(rawMap["d"][1 : len(rawMap["d"])-1])
	if ohlcv.D, err = strconv.ParseInt(v, 10, 64); err != nil {
		return err
	}
	return nil
}

func newOHLCVFromJSONArray(pair string, b []byte) (OHLCV, error) {
	var ohlcv OHLCV
	var array []json.RawMessage
	if err := json.Unmarshal(b, &array); err != nil {
		return ohlcv, err
	}
	ohlcv.Pair = pair
	ts, err := strconv.Atoi(string(array[0]))
	if err != nil {
		return ohlcv, err
	}
	ohlcv.Timestamp = timestamp(time.Unix(int64(ts), 0))
	var value string
	if err = json.Unmarshal(array[1], &value); err != nil {
		return ohlcv, fmt.Errorf("Error unmarshaling open value: %s", err)
	}
	ohlcv.Open, err = strconv.ParseFloat(value, 64)
	if err != nil {
		return ohlcv, err
	}
	if err = json.Unmarshal(array[2], &value); err != nil {
		return ohlcv, fmt.Errorf("Error unmarshaling high value: %s", err)
	}
	ohlcv.High, err = strconv.ParseFloat(value, 64)
	if err != nil {
		return ohlcv, err
	}
	if err = json.Unmarshal(array[3], &value); err != nil {
		return ohlcv, fmt.Errorf("Error unmarshaling low value: %s", err)
	}
	ohlcv.Low, err = strconv.ParseFloat(value, 64)
	if err != nil {
		return ohlcv, err
	}
	if err = json.Unmarshal(array[4], &value); err != nil {
		return ohlcv, fmt.Errorf("Error unmarshaling closed value: %s", err)
	}
	ohlcv.Close, err = strconv.ParseFloat(value, 64)
	if err != nil {
		return ohlcv, err
	}
	if err = json.Unmarshal(array[5], &value); err != nil {
		if err = json.Unmarshal(array[5], &ohlcv.Volume); err != nil {
			return ohlcv, fmt.Errorf("Error unmarshaling volume value: %s", err)
		}
		return ohlcv, nil
	}
	ohlcv.Volume, err = strconv.ParseInt(value, 10, 64)
	if err != nil {
		return ohlcv, fmt.Errorf("error parsing volume: %s '%s' '%s'", err, value, string(array[5]))
	}
	return ohlcv, nil
}
