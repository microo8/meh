package cexio

import (
	"encoding/json"
	"errors"
	"log"

	"github.com/gorilla/websocket"
)

const wsURL = "wss://ws.cex.io/ws/"

//Client the client to the cexio api
type Client struct {
	conn        *websocket.Conn
	key, secret string
	disconnect  chan struct{}
	msgs        chan *msg

	tickers []chan TickMsg //Ticker subscriptions slice
	obs     map[string]chan OrderBook
	ohlcvs  map[string]chan OHLCV
}

//Connect creates an new client that will be connected and authentificated
func Connect(key, secret string) (*Client, error) {
	c := &Client{
		key:    key,
		secret: secret,
		msgs:   make(chan *msg),
	}
	if err := c.connect(); err != nil {
		return nil, err
	}
	return c, nil
}

func (c *Client) connect() error {
	if c.disconnect != nil {
		c.disconnect <- struct{}{}
	}
	conn, _, err := websocket.DefaultDialer.Dial(wsURL, nil)
	if err != nil {
		return err
	}
	var m wsMsg
	if err = conn.ReadJSON(&m); err != nil {
		return err
	}
	if m.E != "connected" {
		conn.Close()
		return errors.New("server didn't send connected message")
	}
	authReq := newAuthRequest(c.key, c.secret)
	if err = conn.WriteJSON(authReq); err != nil {
		conn.Close()
		return err
	}
	var wsmsg wsMsg
	if err = conn.ReadJSON(&wsmsg); err != nil {
		conn.Close()
		return err
	}
	var authResp authResponse
	if err = json.Unmarshal(wsmsg.Data, &authResp); err != nil {
		conn.Close()
		return err
	}
	if authResp.OK != "ok" {
		conn.Close()
		return errors.New("Auth response: " + authResp.Error)
	}
	c.conn = conn
	c.listen()
	return nil
}

func (c *Client) listen() {
	if c.disconnect == nil {
		c.disconnect = make(chan struct{})
	}
	go func() {
		for {
			_, message, err := c.conn.ReadMessage()
			c.msgs <- &msg{
				data: message,
				err:  err,
			}
			if err != nil {
				return
			}

		}
	}()
	go func() {
		for {
			select {
			case <-c.disconnect:
				c.Close()
				return
			case m := <-c.msgs:
				if err := c.handleMsg(m); err != nil {
					log.Fatalf("error handleMsg: %s (%s)", err, string(m.data))
				}
			}
		}
	}()
}

func (c *Client) handleMsg(m *msg) error {
	if m.err != nil {
		log.Fatalf("error: %s %s", m.err, string(m.data))
		return c.connect()
	}
	var wsm wsMsg
	if err := json.Unmarshal(m.data, &wsm); err != nil {
		log.Fatalf("cannot unmarshal data: %s", string(m.data))
	}
	switch wsm.E {
	case "ping":
		return c.conn.WriteJSON(wsMsg{E: "pong"})
	case "disconnecting":
		var dMsg disconnectingMsg
		err := json.Unmarshal(m.data, &dMsg)
		if err != nil {
			return err
		}
		return c.connect()
	case "tick":
		if len(c.tickers) == 0 { //no one listens to tick
			return nil
		}
		var tick TickMsg
		if err := json.Unmarshal(m.data, &tick); err != nil {
			return err
		}
		for _, tickChan := range c.tickers {
			tickChan <- tick
		}
		return nil
	case "order-book-subscribe", "md_update":
		if c.obs == nil {
			return nil
		}
		var ob OrderBook
		if err := json.Unmarshal(wsm.Data, &ob); err != nil {
			return err
		}
		var zeroTime timestamp
		if ob.Timestamp == zeroTime {
			ob.Timestamp = ob.Time
		}
		if ob.Time == zeroTime {
			ob.Time = ob.Timestamp
		}
		obChan, ok := c.obs[ob.Pair]
		if !ok {
			return nil
		}
		obChan <- ob
		return nil
	case "init-ohlcv-data", "ohlcv":
		ohlcvChan, ok := c.ohlcvs[wsm.Pair]
		if !ok {
			return nil
		}
		var array []json.RawMessage
		if err := json.Unmarshal(wsm.Data, &array); err != nil {
			return err
		}
		for _, a := range array {
			ohlcv, err := newOHLCVFromJSONArray(wsm.Pair, a)
			if err != nil {
				return err
			}
			ohlcvChan <- ohlcv
		}
		return nil
	case "ohlcv24":
		return nil
	case "ohlcv1m":
		var ohlcv OHLCV
		if err := json.Unmarshal(wsm.Data, &ohlcv); err != nil {
			return err
		}
		ohlcvChan, ok := c.ohlcvs[ohlcv.Pair]
		if !ok {
			return nil
		}
		ohlcvChan <- ohlcv
		return nil
	case "balance":
		//TODO
		return nil
	default:
		return errors.New("unknown message " + wsm.E)
	}
}

//Close disconnects the client connection
func (c *Client) Close() error {
	c.disconnect <- struct{}{}
	err := c.conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	if err != nil {
		return err
	}
	return c.conn.Close()
}

//Ticker subscription allows you to subscribe to real-time updates of executed trades.
//Notification does not include information like lowest and highest prices, volume, but indicates that trading is happening.
func (c *Client) Ticker() chan TickMsg {
	if len(c.tickers) == 0 { //no subscription has been send
		if err := c.conn.WriteJSON(tickerMsg{
			wsMsg: wsMsg{E: "subscribe"},
			Rooms: []string{"tickers"},
		}); err != nil {
			panic(err)
		}
	}
	tickChan := make(chan TickMsg)
	c.tickers = append(c.tickers, tickChan)
	return tickChan
}

//OrderBook subscription
func (c *Client) OrderBook(cc1, cc2 string) chan OrderBook {
	var req reqOrderBook
	req.Subscribe = true
	req.Depth = -1
	req.Pair = []string{cc1, cc2}
	data, err := wrapMsg("order-book-subscribe", req)
	if err != nil {
		panic(err)
	}
	if err := c.conn.WriteMessage(websocket.BinaryMessage, data); err != nil {
		panic(err)
	}
	obChan := make(chan OrderBook)
	if c.obs == nil {
		c.obs = make(map[string]chan OrderBook)
	}
	c.obs[cc1+":"+cc2] = obChan
	return obChan
}

//OHLCV ...
func (c *Client) OHLCV(cc1, cc2, i string) chan OHLCV {
	var req reqOHLCV
	req.E = "init-ohlcv"
	//req.E = "init-ohlcv-new"
	req.I = i
	req.Rooms = []string{"pair-" + cc1 + "-" + cc2}
	if err := c.conn.WriteJSON(req); err != nil {
		panic(err)
	}
	if c.ohlcvs == nil {
		c.ohlcvs = make(map[string]chan OHLCV)
	}
	ohlcvChan := make(chan OHLCV)
	c.ohlcvs[cc1+":"+cc2] = ohlcvChan
	return ohlcvChan
}
