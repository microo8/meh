package main

//TODO download cache!

import (
	"encoding/gob"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"path"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"unicode"

	"github.com/PuerkitoBio/fetchbot"
	"github.com/PuerkitoBio/goquery"
)

var (
	torrentsDownloaded int64
	downloadedLinks    []string
	linksLock          sync.Mutex
	re                 = regexp.MustCompile("([1-2][0-9][0-9][0-9])")
	imdb               = flag.Float64("imdb", 7.5, "minimal IMDB rating")
	maxTorrents        = flag.Int64("max", 10, "max number of torrents to download")
	contains           = flag.String("contains", "CZ", "full movie name must contain this substring")
	torrentsPath       = flag.String("path", ".", "path where the torrents will be downloaded")
)

func init() {
	loadDownloadedTorrents()
}

func loadDownloadedTorrents() {
	usr, err := user.Current()
	if err != nil {
		log.Panic(err)
	}
	downloadLinksPath := path.Join(usr.HomeDir, ".cache", "cztorrent.gob")
	if _, err = os.Stat(downloadLinksPath); os.IsNotExist(err) {
		return
	}
	file, err := os.Open(downloadLinksPath)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()
	dec := gob.NewDecoder(file)
	err = dec.Decode(&downloadedLinks)
	if err != nil {
		log.Panic(err)
	}
}

func saveDownloadedTorrents() {
	usr, err := user.Current()
	if err != nil {
		log.Panic(err)
	}
	downloadLinksPath := path.Join(usr.HomeDir, ".cache", "cztorrent.gob")
	file, err := os.Create(downloadLinksPath)
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()
	enc := gob.NewEncoder(file)
	err = enc.Encode(downloadedLinks)
	if err != nil {
		log.Panic(err)
	}
}

func addDownloadedTorrents(link string) {
	linksLock.Lock()
	defer linksLock.Unlock()
	downloadedLinks = append(downloadedLinks, link)
	sort.Strings(downloadedLinks)
	saveDownloadedTorrents()
}

func inDownloadedTorrents(link string) bool {
	index := sort.SearchStrings(downloadedLinks, link)
	return index < len(downloadedLinks) && downloadedLinks[index] == link
}

//Command ...
type Command struct {
	url    *url.URL
	method string
}

//NewCommand returns new Command
func NewCommand(curl, method string) *Command {
	command := &Command{}
	res, err := url.Parse(curl)
	if err != nil {
		log.Panic(err)
	}
	command.url = res
	command.method = method
	return command
}

//URL ...
func (c *Command) URL() *url.URL {
	return c.url
}

//Method ...
func (c *Command) Method() string {
	return c.method
}

//Reader returns request body reader
//func (c *Command) Reader() io.Reader {
//	return strings.NewReader("username=microo8&password=69FuTuRe96")
//}

//Cookies ...
func (c *Command) Cookies() (ret []*http.Cookie) {
	return GetCookies(c.url.Host)
}

func main() {
	flag.Parse()
	log.SetFlags(log.Lshortfile)
	f := fetchbot.New(fetchbot.HandlerFunc(handler))
	f.AutoClose = true
	queue := f.Start()
	queue.Send(NewCommand("https://tracker.cztorrent.net/torrents?c1=1&s=&t=0", "GET"))
	queue.Block()
}

func handler(ctx *fetchbot.Context, res *http.Response, err error) {
	if err != nil {
		fmt.Printf("error: %s\n", err)
		return
	}
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Printf("error: %s\n", err)
		return
	}
	var wg sync.WaitGroup
	doc.Find(`.detaily a`).Each(func(i int, s *goquery.Selection) {
		wg.Add(1)
		go fetchTorrents(&wg, s)
	})
	wg.Wait()
	if torrentsDownloaded >= *maxTorrents {
		go func() {
			log.Print("CANCEL!")
			ctx.Q.Cancel() //TODO didn't cancel queue
		}()
		return
	}
	pageNum := ctx.Cmd.URL().Query().Get("p")
	page := 0
	if pageNum != "" {
		page, err = strconv.Atoi(pageNum)
		if err != nil {
			log.Panic(err)
		}
	}
	page++
	log.Println("page", page)
	ctx.Q.Send(NewCommand(fmt.Sprintf("https://tracker.cztorrent.net/torrents?c1=1&s=&t=0&p=%d", page), "GET"))
}

func fetchTorrents(wg *sync.WaitGroup, s *goquery.Selection) {
	defer wg.Done()
	filmTitle := GetFilmTitle(s.Text())
	if filmTitle == "" {
		return
	}
	rating := GetIMDBRating(filmTitle, re.FindString(s.Text()))
	download := s.Parent().Parent().Find(".download a")
	if rating >= *imdb && download.AttrOr("class", "download_dis") != "download_dis" {
		link := fmt.Sprintf("https://tracker.cztorrent.net%s", download.AttrOr("href", ""))
		if !inDownloadedTorrents(link) {
			log.Printf("film: %s IMDB: %f", filmTitle, rating)
			err := downloadTorrent(link)
			if err != nil {
				log.Print(err)
			}
		}
	}
}

//GetFilmTitle crops the full movie title
func GetFilmTitle(filmTitle string) string {
	if !strings.Contains(filmTitle, *contains) {
		return ""
	}
	index := strings.Index(filmTitle, "/")
	if index > 0 {
		filmTitle = strings.TrimSpace(filmTitle[index+1:])
	}
	index = strings.IndexFunc(filmTitle, func(r rune) bool { return !unicode.IsLetter(r) && !unicode.IsSpace(r) })
	if index > 0 {
		filmTitle = strings.TrimSpace(filmTitle[:index])
	}
	return filmTitle
}

//GetIMDBRating gets the imdb rating for the movie TODO film year
func GetIMDBRating(filmTitle string, year string) float64 {
	resp, err := http.Get(fmt.Sprintf(`http://www.omdbapi.com/?t=%s&y=%s&plot=short&r=json`, url.QueryEscape(filmTitle), year))
	if err != nil {
		log.Panic(err)
	}
	obj := make(map[string]string)
	dec := json.NewDecoder(resp.Body)
	dec.Decode(&obj)
	if err != nil {
		log.Panic(err)
	}
	ret, err := strconv.ParseFloat(obj["imdbRating"], 64)
	if err != nil {
		return 0
	}
	return ret
}

func downloadTorrent(torrentURL string) error {
	req, err := http.NewRequest("GET", torrentURL, nil)
	if err != nil {
		return err
	}
	turl, err := url.Parse(torrentURL)
	if err != nil {
		return err
	}
	for _, c := range GetCookies(turl.Host) {
		req.AddCookie(c)
	}
	res, err := (&http.Client{}).Do(req)
	if err != nil {
		return err
	}

	filename := res.Header.Get("Content-Disposition")
	index := strings.Index(filename, "\"")
	if index != -1 {
		filename = filename[index+1 : len(filename)-1]
	} else {
		paths := strings.Split(turl.Path, "/")
		filename = paths[len(paths)-1] + ".torrent"
	}

	file, err := os.Create(path.Join(*torrentsPath, filename))
	if err != nil {
		log.Panic(err)
	}
	defer file.Close()
	io.Copy(file, res.Body)
	log.Println("DOWNLOADED", filename, atomic.AddInt64(&torrentsDownloaded, 1))
	addDownloadedTorrents(torrentURL)
	return nil
}
