// +build android

package mypkg

import (
	"Java/android/os"
	"Java/android/app"
	"Java/android/app/Activity"
	gopkg "Java/reverse"
)

type MainActivity struct {
	app.Activity
	x int
}

func (a *Main) OnCreate(this gopkg.MainActivity, b os.Bundle) {
	this.Super().OnCreate(b)
}
